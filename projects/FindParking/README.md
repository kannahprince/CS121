# Project 2: Find Parking
* Name: Prince Kannah
* Class: CS121
* Date: 2/24/15

## Overview:
FindParking is an application that randomly generates four parking spots and determines which parking spot is the closest to your current location.

## Included files:
The directory tree below shows all the required files for this project:
    
    ├── README.md
    └── src
        ├── FindParking.java
        ├── FindParkingV2.java
        └── ParkingSpot.java

## Building & Running:
**_NOTE_: All program files must be in the same directory.**
* To compile the program (using terminal):
	* `javac FindParking.java`

* To run the compiled program (using terminal):
	* `java FindParking`

## Program Design:
The ParkingSpot class is use to instantiate parking spot objects with randomly generated X & Y coordinates. The FindParking class then uses those object's APIs to calculate costs, distances and in turn find the closest parking spot the current location.
