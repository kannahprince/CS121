import java.text.NumberFormat;
import java.util.Random;
import java.util.Scanner;

public class FindParkingV2
{

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        final int BOUND = 100;

        System.out.print("Enter a seed: ");
        long seed = scan.nextLong();
        Random rand = new Random(seed);

        System.out.print("Enter parking time (in minutes): ");
        int parkingTime = scan.nextInt();
        scan.close();

        //add space
        System.out.println();

        ParkingSpot sp1 = new ParkingSpot("5th Ave.",rand.nextInt(BOUND),
                rand.nextInt(BOUND));
        ParkingSpot sp2 = new ParkingSpot("Broadway",rand.nextInt(BOUND),
                rand.nextInt(BOUND));
        ParkingSpot sp3 = new ParkingSpot("Wall St.",rand.nextInt(BOUND),
                rand.nextInt(BOUND));
        ParkingSpot sp4 = new ParkingSpot("Lake Shore Dr.",rand.nextInt(BOUND),
                rand.nextInt(BOUND));

        sp3.setCharge(0.3);
        sp4.setCharge(0.3);

        int driverX = rand.nextInt(BOUND);
        int driverY = rand.nextInt(BOUND);

        int sp1Distance = sp1.getDistance(driverX,driverY);
        int sp2Distance = sp2.getDistance(driverX,driverY);
        int sp3Distance = sp3.getDistance(driverX,driverY);
        int sp4Distance = sp4.getDistance(driverX,driverY);

        double sp1Cost = sp1.getCost(parkingTime);
        double sp2Cost = sp2.getCost(parkingTime);
        double sp3Cost = sp3.getCost(parkingTime);
        double sp4Cost = sp4.getCost(parkingTime);

        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        System.out.println("Parking Spot 1: " + sp1 + "\n\tdistance = "
                + sp1Distance + " cost = " + fmt.format(sp1Cost));
        System.out.println("Parking Spot 2: " + sp2 + "\n\tdistance = "
                + sp2Distance + " cost = " + fmt.format(sp2Cost));
        System.out.println("Parking Spot 3: " + sp3 + "\n\tdistance = "
                + sp3Distance + " cost = " + fmt.format(sp3Cost));
        System.out.println("Parking Spot 4: " + sp4 + "\n\tdistance = "
                + sp4Distance + " cost = " + fmt.format(sp4Cost));

        int minOne = Math.min(sp1Distance,sp2Distance);
        int minTwo = Math.min(sp3Distance,sp4Distance);

        ParkingSpot temp = null;
        if(Math.min(minOne,minTwo) == sp1Distance)
        {
            temp = sp1;
        }
        else if(Math.max(minTwo,minOne) == sp2Distance)
        {
            temp = sp2;
        }
        else if(Math.min(minTwo,minOne) == sp3Distance)
        {
            temp = sp3;
        }
        else
        {
            temp = sp4;
        }
        System.out.println(
                "\nPosition of vehicle x = " + driverX + " y = " + driverY);
        System.out.println(
                "Distance of the cloeset spot: " + Math.min(minOne,minTwo));
        System.out.println("Cloest spot: " + temp);
    }

}
