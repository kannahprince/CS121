import java.text.NumberFormat;
import java.util.Random;
import java.util.Scanner;

public class FindParking
{
    private final static int NUMBER_PARKING = 4;
    private final static int BOUND = 100;
    private static final String[] streetNames = {"8th St","Boise Ave.",
            "University Dr.","Bannock St"};
    private static Random rand;
    private static Scanner scan;

    public static void main(String[] args)
    {
        //initialize the scanner to read from keyboard
        scan = new Scanner(System.in);

        //create array to hold our ParkingSpot object. Aptly named garage.
        ParkingSpot[] garage = new ParkingSpot[NUMBER_PARKING];

        //setting up the different charge to use later
        double myCharge = 0.30;

        //ask for & store Random seed.
        System.out.print("Enter random seed: ");
        rand = new Random(scan.nextLong());

        //ask for & store the parking time
        System.out.print("Enter parking time (in minutes): ");
        int parkingTime = scan.nextInt();

        //fill the garage with new ParkingSpots
        for(int i = 0; i < garage.length; i++)
        {
            garage[i] = new ParkingSpot(streetNames[i],rand.nextInt(BOUND),
                    rand.nextInt(BOUND));
        }

        //generate the driver's starting position
        int driverX = rand.nextInt(BOUND);
        int driverY = rand.nextInt(BOUND);

        //update charge to 30 cents for the last 2 spots
        garage[2].setCharge(myCharge);
        garage[3].setCharge(myCharge);

        System.out.println();//add space
        //calculate the distance and cost
        for(int i = 0; i < garage.length; i++)
        {
            System.out.println(
                    "Parking Spot " + i + ": " + garage[i] + "\n\t distance = "
                            + garage[i].getDistance(driverX,driverY)
                            + " cost = " + NumberFormat.getCurrencyInstance()
                                    .format(garage[i].getCost(parkingTime)));
        }
        System.out.println();//add space

        //figure out the closest distance to the driver
        int min = Math.min(
                Math.min(garage[0].getDistance(driverX,driverY),
                        garage[1].getDistance(driverX,driverY)),
                Math.min(garage[2].getDistance(driverX,driverY),
                        garage[3].getDistance(driverX,driverY)));

        ParkingSpot temp = null;
        for(int i = 0; i < garage.length; i++)
        {
            if(garage[i].getDistance(driverX,driverY) == min)
            {
                temp = garage[i];
            }
        }

        //print it out
        System.out.println(
                "Position of the vehicle: x = " + driverX + " y = " + driverY);
        System.out.println("Distance of closest spot: "
                + temp.getDistance(driverX,driverY));
        System.out.println("Closest spot: " + temp);

    }

}
