import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;

/**
 * Created by princekannah on 4/7/16.
 * Working project: Jukebox.
 */

@SuppressWarnings("WeakerAccess")
public class Song {
    private String title, artist, fileName;
    private int playTime, playCount;
    private AudioClip clip;

    public Song(String title, String artist, int playTime, String fileName) {
        this.title = title;
        this.artist = artist;
        this.playTime = playTime;
        this.fileName = fileName;

        String fullPath = new File(fileName).getAbsolutePath();
        try {
            this.clip = Applet.newAudioClip(new URL("file:" + fullPath));
        } catch (Exception e) {
            System.out.println("Error loading sound clip for " + fullPath);
            System.out.println(e.getMessage());
        }
    }

    public Song(String title, String artist) {
        this.title = title;
        this.artist = artist;
    }

    public int getPlayCount() {
        return playCount;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPlayTime(int playTime) {
        this.playTime = playTime;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * Plays this song asynchronously.
     */
    public void play() {
        if (clip != null) {
            clip.play();
            playCount++;
        }
    }

    /**
     * Stops this song from playing.
     */
    public void stop() {
        if (clip != null) {
            clip.stop();
        }
    }

    public String getTitle() {
        return title;
    }

    public String getFileName() {
        return fileName;
    }

    public String getArtist() {
        return artist;
    }

    public int getPlayTime() {
        return playTime;
    }

    /* (non-Javadoc)
      * @see java.lang.Object#toString()
      */
    @Override
    public String toString() {
        return String.format("%-20s %-20s %-25s %10d",
                title, artist, fileName, playTime);
    }

    private String formatPlayTime() {
        StringBuilder builder = new StringBuilder(4);
        DecimalFormat format = new DecimalFormat("##:00");
        int toMinutes = ((getPlayTime() % 3600) / 60);
        int toSeconds = (getPlayTime() % 60);
        builder.append(format.format(toMinutes));
        builder.append(format.format(toSeconds));

        return builder.toString().substring(0, 5);
    }

    public String getFilePath() {
        return fileName;
    }

    public void setFilePath(String filePath) {
        this.fileName = filePath;
    }
}
