import java.util.ArrayList;

/**
 * Created by princekannah on 4/7/16.
 * Working project: Jukebox.
 */
@SuppressWarnings("WeakerAccess")
public class PlayList {
    private ArrayList<Song> songList;
    private Song playing;
    private String name;

    public PlayList(String name) {
        this.name = name;
        songList = new ArrayList<>();
        playing = null;
    }

    public void addSong(Song s) {
        songList.add(s);
    }

    public Song getSong(int i) {
        if (i >= 0 && i < songList.size())
            return songList.get(i);
        return null;
    }

    public void playSong(int i) {
        if (i >= 0 && i < getNumSongs()) {
            songList.get(i).play();
            playing = songList.get(i);
        }
    }

    public String getInfo() {
        if (getNumSongs() == 0)
            return "There are no songs.";
        StringBuilder sb = new StringBuilder();
        double average = (double) getTotalPlayTime() / songList.size();
        sb.append("The average play time is: " + String.format("%.2f", average) + " seconds\n");
        sb.append("The shortest song is: " + findShortest() + "\n");
        sb.append("The longest song is: " + findLongest() + "\n");
        sb.append("Total play time: " + getTotalPlayTime() + "\n");

        return sb.toString();
    }

    private Song findLongest() {
        Song temp = new Song("", "", Integer.MIN_VALUE, "");
        for (Song s : songList) {
            if (temp.getPlayTime() < s.getPlayTime())
                temp = s;
        }
        return temp;
    }

    private Song findShortest() {
        Song temp = new Song("", "", Integer.MAX_VALUE, "");
        for (Song s : songList) {
            if (s.getPlayTime() < temp.getPlayTime())
                temp = s;
        }
        return temp;
    }

    public Song getPlaying() {
        return playing;
    }

    public Song removeSong(int id) {
        if (id < songList.size() && id >= 0)
            return songList.remove(id);
        return null;
    }

    public int getNumSongs() {
        return songList.size();
    }

    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getTotalPlayTime() {
        int total = 0;

        for (Song s : songList) {
            total += s.getPlayTime();
        }
        return total;
    }

    public ArrayList<Song> getSongList() {
        return songList;
    }

    @Override
    public String toString() {
        String tacks = "------------------\n";
        String index;
        int i = 0;
        StringBuilder builder = new StringBuilder();
        builder.append(tacks).append(getName()).
                append(" (").append(getNumSongs()).append(" songs)\n").append(tacks);

        if (getNumSongs() > 0) {
            for (Song s : songList) {
                index = "(" + i + ")";
                builder.append(index).append(" ").append(s.toString()).append("\n");
                i++;
            }
        } else {
            builder.append("There are no songs.\n");
        }
        builder.append(tacks);
        return builder.toString();
    }
}
