# Project 1: Traffic Animation
* Name: Prince Kannah
* Class: CS121
* Date: 2/10/15

## Overview:
Traffic animation is a program that animates a moving car across the screen.

## Included files:
The included project files are listed in the directory tree below:
    
    ├── README.md
    └── src
        └──TrafficAnimation.java

## Building & Running:
**_NOTE_: All program files must be in the same directory.**
* To compile the program (using terminal):
	* `javac TrafficAnimation.java`

* To run the compiled program (using terminal):
	* `java TrafficAnimation`

## Program Design:
Everything is done in the TrafficAnimation class. [Java's AWT](http://docs.oracle.com/javase/8/docs/api/java/awt/package-summary.html) is use to draw shapes, add text and colors. The [Swing APIs](http://docs.oracle.com/javase/8/docs/api/javax/swing/package-summary.html) were use to create an animation thread that ran periodically to simulate the car moving across traffic.
