
/*
 * TrafficAnimation.java CS 121 Project 1: Traffic Animation
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import java.util.Random;

/**
 * Animates a car traveling down a lane.
 * @author [Prince Kannah]
 */
@SuppressWarnings("serial")
public class TrafficAnimation extends JPanel
{
    //Note: This area is where you declare constants and variables that
    //	need to keep their values between calls	to paintComponent().
    //	Any other variables should be declared locally, in the
    //	method where they are used.

    //constant to regulate the frequency of Timer events
    // Note: 100ms is 10 frames per second - you should not need
    // a faster refresh rate than this
    private final int DELAY = 100;//milliseconds
    //anchor coordinate for drawing / animating
    private int x = 0;
    //pixels added to x each time paintComponent() is called
    private int stepSize = 10;

    /*
     * This method draws on the panel's Graphics context. This is where the
     * majority of your work will be.
     *
     * (non-Javadoc)
     * 
     * @see java.awt.Container#paint(java.awt.Graphics)
     */
    public void paintComponent(Graphics canvas)
    {

        //account for changes to window size
        int width = getWidth();// panel width
        int height = getHeight();// panel height

        //generate random colors for my vehicle
        Random rand = new Random();
        int red = rand.nextInt(256);
        int green = rand.nextInt(256);
        int blue = rand.nextInt(256);
        Color randC = new Color(red,green,blue);

        //my color
        Color myYellow = new Color(255,255,0);

        //scaling
        int squareSide = height / 5;
        int y = ((height / 2) - (squareSide / 2));

        //Fill the canvas with the background color
        canvas.setColor(getBackground());
        canvas.fillRect(0,0,width,height);

        //road
        canvas.setColor(Color.gray);
        canvas.fillRect(0,height / 4,width,height / 2);

        //medians
        canvas.setColor(myYellow);
        canvas.fillRect(0,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 2,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 4,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 6,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 8,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 10,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 12,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 14,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 16,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 18,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 20,(height / 2),squareSide,squareSide / 5);
        canvas.fillRect(squareSide * 22,(height / 2),squareSide,squareSide / 5);

        //Calculate the new position & smooth wrapping
        x = (x + stepSize) % (width * (2));
        int carWidth = (3 * squareSide);
        int carX = (x - (1) * carWidth);

        //Draw new square
        canvas.setColor(randC);
        canvas.fillRect(carX,y + 10,carWidth,squareSide);//strobe car
        canvas.setColor(Color.green);

        canvas.fillOval(carX,y + squareSide,squareSide / 2,squareSide / 2);//first wheel
        canvas.fillOval((carX + 2 * squareSide),(y + squareSide),squareSide / 2,
                squareSide / 2);//second wheel

        //centipede avatar
        canvas.setColor(Color.CYAN);
        int firstHeadX = (squareSide / 8);
        int firstHeadY = (height - (height / 4));
        canvas.fillOval(firstHeadX,firstHeadY,squareSide,squareSide);//first head
        canvas.fillOval(firstHeadX + 80,firstHeadY,squareSide / 2,
                squareSide / 2);
        canvas.fillOval(firstHeadX + 160,firstHeadY,squareSide / 3,
                squareSide / 3);

        canvas.setColor(Color.YELLOW);
        canvas.fillOval(-40,-40,squareSide,squareSide);//sun

        //MY TEXT
        canvas.setFont(new Font("Courier New",Font.BOLD,20));
        String myText = "Oh no, I'm going to miss the bus!";
        canvas.setColor(Color.magenta);
        canvas.drawString(myText,squareSide,squareSide);
    }

    /**
     * Constructor for the display panel initializes necessary variables. Only
     * called once, when the program first begins. This method also sets up a
     * Timer that will call paint() with frequency specified by the DELAY
     * constant.
     */
    public TrafficAnimation()
    {
        setBackground(Color.black);
        //Do not initialize larger than 800x600
        int initWidth = 800;
        int initHeight = 600;
        setPreferredSize(new Dimension(initWidth,initHeight));
        this.setDoubleBuffered(true);

        //Start the animation - DO NOT REMOVE
        startAnimation();
    }

    /////////////////////////////////////////////
    // DO NOT MODIFY main() or startAnimation()//
    /////////////////////////////////////////////

    /**
     * Starting point for the TrafficAnimation program
     * @param args unused
     */
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("Traffic Animation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new TrafficAnimation());
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Create an animation thread that runs periodically DO NOT MODIFY this
     * method!
     */
    private void startAnimation()
    {
        ActionListener taskPerformer = new ActionListener() {

            public void actionPerformed(ActionEvent event)
            {
                repaint();
            }
        };
        new Timer(DELAY,taskPerformer).start();
    }
}
