# P4: Analyzing Text Files
* Name: Prince Kannah
* Class: CS121
* Date: 04/18/15

## Overview:
ProcessText is a command line program that takes any number of text files then uses TextStatistics to parse and collect the number of lines, words, the frequency of a word length, average word length and the occurrence of each letter in the file.

## Included Files:
The included files for this project are listed in the directory tree below:
    
    ├──README.md
    ├──etext
    │   ├──Alice-in-Wonderland.txt
    │   ├──Bill.txt
    │   ├──Clinton-93-speech.txt
    │   ├──Gettysburg-Address.txt
    │   ├──Patrick-Henry.txt
    │   ├──US-Constitution.txt
    │   ├──WordsToParseBy.txt
    │   ├──XP-License.txt
    │   └──testfile.txt
    └── src
        ├──ProcessText.java
        ├──TextStatistics.java
        ├──TextStatisticsInterface.java
        └──TextStatisticsTest.java

## Building & Running:
**_NOTE_: All program files must be in the same directory**
* To compile the program (using terminal):
	* `javac ProcessText.java`

* To run the compiled program (using terminal):
	* `java ProcessText <file1> <file2>...`

* To use TextStatisticsTest through the terminal, first compile then run:
	* `javac TextStatisticsTest.java`

	* `java TextStatisticsTest`
	
## Program Design:
 The backbone of ProcessText is the TextStatistics class, which implements the TextStatisticsInterface. A file is created in ProcessText then passed to TextStatistics. Once TextStatistics has a file, it opens a Scanner to read each line. The line count is incremented with each line and since the line includes all the characters in it, the character count is line count +1. The +1 accounts for the new line that is skipped by the Scanner. As each line is read, StringTokenizer is use to get individual words increasing word count with each tokenized words. The letter and word length counts are increased after checking if it's a letter and word length respectively at this level.

## Program Development:

#### Divide & Conquer:

I started working on TextStatistics after setting up my project in Eclipse. I first implemented the interface and that served as my guide. After a few failed attempts of trying to do everything at once, I broke down the project into two parts. First I focused on my line, word and character count since those didn't involve arrays and seemed simple. I knew early on I'd need to have several Scanners going as the program starts from a line then drills down to the character level. Line count wasn't hard to figure out but I found character count to be a bit trickier. I Initially counted characters at the word level because I figured since I now have a word, I can go through it and count each individual letter. The problem with this was that at this point, I haven't used [StringTokenizer](http://docs.oracle.com/javase/8/docs/api/java/util/StringTokenizer.html) to tokenized words, removing all special characters and spaces. A post on Piazza later and it made sense to collect the character count at the line level since that includes all the characters. Something I learned is that Scanner skips the new line character so I needed to make up for that by adding one with each line.

After that, I turned to my letter and word length count arrays. Having a max word length helped with the word length array as it simplifies things just a little. Since each index represents a word length that index needs to be incremented every time I come across a word of that length. While this does make things simpler, it doesn't truly account for word lengths beyond 23. That would be a great feature to add. The letter count kind of followed the same idea except I needed to check to make sure the current character is a letter and subtract from its ASCII value to get the correct index.

 
#### The Importance of `+=`:

This thus far has been one of the most complicated toString I've written and learned some important lessons along the way. In my letter count loop I needed to concatenate the letter with its count but for 2 days my code was written as tmp = str. Running it inside the loop I only get the last index, Z. I moved it outside the loop but only got A this time. I was beyond ecstatic once I finally saw and fixed the mistake (should have been tmp += str). This toString() reminded me of P1 as the order I placed new lines, tabs and spaces were important just as the orders of drawings and colors in P1. One thing I really wanted to do was making the formatting dynamic, with the double equal and hyphen adjust accordingly based on the output. Will need to research for future updates.

## Testing:

I started testing once I was sure everything was working properly and the output formatted correctly. I ran pretty much every text file provided and it was fun seeing all the statistics on each one. After running all of those I thought about other format and experimented with .docx (Word 2013) and PDF but those didn't work (probably because they're propriety format). Adding a variety of file support would be wicked awesome!! Not content I thought:
>Hey, why not scan all of Wikipedia!

I found a text copy of the English version of Wikipedia but it was too big (47GB!). If only I had Google Fiber. I finally ran TextStatisticsTest and the shell script and both passed with flying colors.

## Extra credit:

TextStatistics has the algorithm use to find the longest word and the line at which it occurs. I compare the longest word (initially an empty String) to the current word to make sure they're are the same length and if so add the line number to [ArrayList](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html). This way, only the first word of same length is tracked. If they're not equal in length, I check to see if the current word is greater than the longest word in length. If that is the case, the current word becomes the longest word, the line numbers ArrayList is cleared since a new word has been found. Once the array is cleared, the current line containing the word is added. This information is printed along with all the other collected statistics.
