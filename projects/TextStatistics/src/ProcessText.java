import java.io.File;

/**
 * ProcessText parses a file and collect some statistics.
 * 
 * @author Prince Kannah
 */

public class ProcessText {

	/**
	 * Takes a number of files to parse
	 * 
	 * @param args
	 *            The name of files to parse.
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Usage: java ProcessText file1 [file2 ...]");
			System.exit(1);
		}

		for (int i = 0; i < args.length; i++) {
			TextStatistics text = new TextStatistics(new File(args[i]));
			System.out.println(text);
		}
	}
}