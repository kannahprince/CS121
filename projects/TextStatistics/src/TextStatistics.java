import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

/**
 * <code>TextStatistics</code> parses a given text file and collects the
 * following statistics: number of lines, words, characters, occurrence of each
 * letter and average word length.
 * <p>
 * Below is an example of how <code>TextStatistics</code> can be created.
 * <blockquote>
 * 
 * <pre>
 * 
 * TextStatistics text = new TextStatistics(file);
 * </pre>
 * 
 * </blockquote>
 * </p>
 * Here is an example of how TextStatistics can be used:
 * <p>
 * <blockquote>
 * 
 * <pre>
 * System.out.println("Word count for file: " + file.getWordCount());
 * System.out.println(file);
 * </pre>
 * 
 * </blockquote>
 * </p>
 * 
 * @author Prince Kannah
 */

public class TextStatistics implements TextStatisticsInterface {
	private static final String DELIMITERS = "[\\W\\d_]+";
	private int charCount, wordCount, lineCount;
	private final int MAX_WORD_LENGTH = 23;// assuming max length is 23
	private final int ALPHABET_LENGHT = 26;

	// to hold array counts
	private int[] letterCount;
	private int[] wordLengthCount;

	// use to Scan file -> line -> word
	private File file;

	// use to find line at which the longest word appears
	private String longestWord;
	private ArrayList<Integer> lineNumbers;

	/**
	 * Constructor: Create a new TextStatistics object with the given file.
	 * 
	 * @param file
	 *            The file to parse
	 */
	public TextStatistics(File file) {
		charCount = wordCount = lineCount = 0;

		wordLengthCount = new int[MAX_WORD_LENGTH + 1];
		letterCount = new int[ALPHABET_LENGHT];
		lineNumbers = new ArrayList<Integer>();
		longestWord = "";
		this.file = file;

		// read and parse the give file
		readFile();
	}

	/**
	 * A helper method use to read and collection the data on the file
	 */
	private void readFile() {
		Scanner fileScan;

		// open & read file
		try {
			fileScan = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.err.println(file + " could not be found.");
			System.exit(1);
			return;
		}

		while (fileScan.hasNextLine()) {
			// scan each line & increase line count
			String line = fileScan.nextLine();
			lineCount++;

			charCount += line.length() + 1;// the length of the line includes
											// all the characters in that line.
											// +1 for the skipped new line char.

			Scanner lineScan = new Scanner(line);
			lineScan.useDelimiter(DELIMITERS);

			while (lineScan.hasNext()) {
				String word = lineScan.next();
				wordCount++;
				// find line with longest word
				if (longestWord.length() == word.length()) {
					lineNumbers.add(lineCount);
				} else if (longestWord.length() <= word.length()) // new longest
																	// word
				{
					longestWord = word;
					lineNumbers.clear();
					lineNumbers.add(lineCount);
				}

				// increase word length count
				if (word.length() <= MAX_WORD_LENGTH) {
					wordLengthCount[word.length()]++;
				} else {
					wordLengthCount[MAX_WORD_LENGTH]++;
				}

				// increase letterCount
				word = word.toLowerCase();
				for (int i = 0; i < word.length(); i++) {
					// make sure we're only looking at letters and not other
					// characters
					if (word.charAt(i) >= 'a' && word.charAt(i) <= 'z') {
						letterCount[word.charAt(i) - 'a']++;
					}
				}
			} // end of while(tokenizer.hasMoreTokens())
			lineScan.close();
		} // end of while(fileScan.hasNextLine())
		fileScan.close();
	}

	@Override
	public int getCharCount() {
		return charCount;
	}

	@Override
	public int getWordCount() {
		return wordCount;
	}

	@Override
	public int getLineCount() {
		return lineCount;
	}

	@Override
	public int[] getLetterCount() {
		return letterCount;
	}

	@Override
	public int[] getWordLengthCount() {
		return wordLengthCount;
	}

	@Override
	public double getAverageWordLength() {
		double sum = 0;
		for (int i = 0; i < wordLengthCount.length; i++) {
			sum += wordLengthCount[i] * i; // (length * number of words at that length)
		}
		return sum / wordCount;
	}
	
	/**
	 * Get the lines that the longest word appears on
	 * @return an array containing all lines with the longest word
	 */
	public Integer[] getLongestLineNumber(){
		return (Integer[]) lineNumbers.toArray();
	}

	/**
	 * @return The statistics of the file.
	 */
	@Override
	public String toString() {
		StringBuilder tempString = new StringBuilder("");
		char a = 'a', n = 'n';
		String doubleEqual = "============================================================ \n";
		String hyphen = "------------------------------ \n";

		for (int i = 0; i < letterCount.length / 2; i++) {
			tempString.append(" " + (a++) + " = " + letterCount[i]);

			tempString.append("  \t" + (n++) + " = " + letterCount[i + 13] + "\n");
		}

		StringBuilder wordCount = new StringBuilder("");
		for (int i = 0; i < wordLengthCount.length; i++) {
			if (wordLengthCount[i] != 0) {
				wordCount.append(String.format("%6d%11d\n", i, wordLengthCount[i]));
			}
		}

		return "Statistics for " + file + "\n" + doubleEqual + getLineCount() + " lines\n" + getWordCount() + " words\n"
				+ getCharCount() + " characters\n" + hyphen + tempString.toString() + hyphen + "Length \t"
				+ "Frequency\n" + "------" + "\t--------- \n" + wordCount.toString() + "\nAverage word length = "
				+ String.format("%.2f", getAverageWordLength()) + "\n" + "The longest line is: "
				+ lineNumbers.toString() + "\n" + doubleEqual;
	}
}