# P5: MineWalker (Texas Ranger)
* Name: Prince Kannah
* Class: CS121

## Overview:
MineWalker is a simple Mine Sweeper clone. The objective is to traverse the grid from the lower right corner to the upper left corner hitting as few mines as possible. Game settings include difficulty level and grid size. 
## Game-play:
* The game starts with these default settings:
	* Difficulty: easy
	* Grid size: 10
	* Points: 500
	* Lives: 5
* On each turn you click a tile to move to it. If it is a valid move then you get 10 points.
* You lose 1 life and 5 points for clicking on a tile that contains a mine. You continue from your previous tile in this case.
* The current tile can be use to help determine the number of nearby mines according to the listed color code:
	* red: 3 mines nearby
	* orange: 2 mines nearby
	* yellow: 1 mines nearby
	* green: 0 mines nearby

* The game is over when you've either successfully made it to the upper left corner or lose all your lives.
* The game also ends when, while in-game, you click either the show path or mines button. 

## Included files:
All project files are listed in the directory tree below:
    
    ├──README.md
    ├──assets
    │   ├──Bayhem.gif
    │   ├──DemomanTaunt.wav
    │   ├──Fanfare.wav
    │   ├──KLoggins.wav
    │   ├──TCrews.wav
    │   ├──TROLL2.wav
    │   ├──happyFace.png
    │   └──sad.png
    └── src
        ├──ButtonCommands.java
        ├──ControlPanel.java
        ├──GameState.java
        ├──GameStatsPanel.java
        ├──GridPanel.java
        ├──MineWalker.java
        ├──MineWalkerPanel.java
        └──RandomWalk.java

## Building & Running:
**_NOTE_: All program files must be in the same folder.**
* To compile the program (using terminal):
	* `javac MineWalker.java`

* To run the compiled program (using terminal):
	* `java MineWalker`

## Program Design:
The most important class is MineWalkerPanel. It handles the creation of all other panels and also manages the game logic. It's a fairly busy class so helper methods are used extensively to do panel creation and setup. In order for the mines to be placed a modified RandomWalk object is created. This modified version starts from the lower right and makes a path to the upper left. The RandomWalk object is use to always guarantee that there's a valid path on the grid as mines are place around but _never_ on the path.

The MineWalkerPanel class is a cluttered class doing way too much. Ideally it should be modular and even employ the [MVC](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) design pattern. This would be a great enhancement down the road.

#### Update (3/26/16):
---
I have broken up the program a bit more with each of the 3 main panels being their own classes. The GridPanel class now creates and manages just the mine field and game logic. The control panel handles
the logic of users starting and stopping game as well as hiding and show paths and mines. A lot of new code was not added. Simply moving refactoring and compartmentalization of the various 
classes.

## Program Development & Testing:
The first thing I did for this project was sketch a layout of my UI. I knew I'd have to use a [BorderLayout](http://docs.oracle.com/javase/tutorial/uiswing/layout/border.html) in order to have all my other panels be on screen at the same time. Once I was happy with how it looked I did a mock-up in Java to see what I'd look like. Satisfied with the results I moved onto implementing my different panels that would hold game information and settings.

I started with the center panel which in turn housed my grid panel. I started with this one because it was very important that the grid panel worked and display information correctly as players would be spending a great deal of time on it. Once it was done I then turn my focus to the north panel. The north panel is the control panel that game settings including grid size, difficulty and starting and/or quitting a game as well as showing/hiding mines and paths. The final panel was the west one, use to display game play information including total points, number of lives and color key.

Testing was a constant part of this project. With each new modification (visual or game wise) I'd run my program to see the results and going back to fix it if it didn't quite look right. One of the biggest mistake I ran into was NullPointer exceptions. As the program got more complex the error got more prevalent. So, as good practice, I always initialize my variables to some values.
## Extra Credit:
As required for the extra credits, user can adjust game difficulty and there are different audio effects throughout the game.

## Known Issues:
* The X that is use to indicate the current tile stays after moving on.
    * Due to how the animation timer is currently setup, depending on when you leave the current
    title, the X (and also the color) stays. For example, if the current tile is blinking yellow X and you
    happen to click on another tile at that moment, the previous tile stays yellow with an X. No impact on progression.
