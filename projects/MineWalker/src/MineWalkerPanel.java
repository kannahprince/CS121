import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.*;
import javax.swing.*;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.Timer;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * <code>MineWalkerPanel</code> creates a Java GUI and manages the logic for a
 * Mine Sweeper clone. JButtons are created on a specified grid size and a
 * RandomWalk object is use to ensure a path around the mines.
 * @author Prince Kannah
 */

@SuppressWarnings("serial")
public class MineWalkerV1 extends JPanel implements ActionListener
{

    //to display playing grid
    private JPanel gridPanel;
    private Point current;// keep track of my current position
    private JButton[][] gridButtons;
    private final int DEFAULT_GRID = 10;
    private int currentGrid;
    private final int DELAY = 1000;// milliseconds

    //sub-panel for game controls
    private JButton showPath;
    private JButton showMines;
    private JButton newGame;
    private JTextField gridTextField;

    private JComboBox<String> diffSelection;

    //sub-panel to display game stats
    //    private JPanel gameStatsPanel;
    private JLabel lifeLabel;
    private JLabel scoreLabel;
    private final int LIFE_DEDUCTION = 5;// points lost per mine
    private final int SCORE_ADDITIVE = 10;// points added to score for guessing
                                          // correctly
    private int scoreCount = 500;// initial score
    private int livesCount = 5;// initial number of lives

    //use with RandomWalk object in in initMinePanel()
    private ArrayList<Point> mines;
    private ArrayList<Point> path;
    //no extra points for back tracking
    private ArrayList<Point> alreadyClicked;

    // for fun
    AudioClip clip;
    private final ImageIcon EXPLOSIONS = new ImageIcon(
            "assets" + File.separator + "Bayhem.gif");

    private final String[] SOUNDCLIPS = {"TCrews.wav","DemoTaunt.wav",
            "Fanfare.wav","KLoggins.wav","TROLL2.wav"};

    /**
     * Use to represent the 2 states the game can end in
     */
    private enum GameState
    {
        NO_LIVES, SUCCESSFUL_ARRIVAL//player makes it to (0,0)
    };

    private enum ButtonCommands
    {
        SHOW_MINES, HIDE_MINES, SHOW_PATH, HIDE_PATH
    };

    private GameState gameState;

    /**
     * Constructor: Sets up the sub-panels and adds them to primary panel.
     */
    public MineWalkerPanel()
    {
        setLayout(new BorderLayout());

        // setting up main components
        initControlPanel();
        currentGrid = Integer.parseInt(gridTextField.getText());
        initMinePanel();
        initGameStatsPanel();
        disableGrid();// forces player to click "NEW GAME" to start playing

        JOptionPane.showMessageDialog(this,
                "DO NOT use headphones on high volume.","HIGH VOLUME",
                JOptionPane.WARNING_MESSAGE);

        startAnimation();
    }

    // ALL PRIVATE CLASS METHODS
    /**
     * Checks if move is valid
     * @param x The x coordinate of the pushed button.
     * @param y The y coordinate of the pushed button.
     * @return Returns <code>true</code> if move is valid; <code>false</code>
     *         otherwise.
     */
    private boolean validMove(int x, int y)
    {
        if(x == current.x && y == current.y - 1)
        {
            return true;
        }
        else if(x == current.x && y == current.y + 1)
        {
            return true;
        }
        else if(x == current.x + 1 && y == current.y)
        {
            return true;
        }
        else if(x == current.x - 1 && y == current.y)
        {
            return true;
        }
        return false;
    }

    /**
     * Check adjacent buttons for mine to the color key.
     * @param x the x coordinate for current button
     * @param y the y coordinate for current button
     */
    private int getNumMines(int x, int y)
    {
        int numMines = 0;

        if(mines.contains(new Point(x,y - 1)))
        {// up
            numMines++;
        }
        if(mines.contains(new Point(x,y + 1)))
        {// down
            numMines++;
        }
        if(mines.contains(new Point(x - 1,y)))
        {// left
            numMines++;
        }
        if(mines.contains(new Point(x + 1,y)))
        {// right
            numMines++;
        }
        return numMines;
    }

    /**
     * Changes the color of the current tile based on the number of surrounding
     * mines
     * @param x - the x coordinate of the current tile
     * @param y - the y coordinate of the current tile
     * @param numMines - the number of mines surrounding the tile
     */
    private void changeColor(int x, int y)
    {
        switch (getNumMines(x,y))
        {
            case 0:
                gridButtons[x][y].setBorder(
                        BorderFactory.createLoweredSoftBevelBorder());
                gridButtons[x][y].setBackground(Color.GREEN);
            break;
            case 1:
                gridButtons[x][y].setBorder(
                        BorderFactory.createLoweredSoftBevelBorder());
                gridButtons[x][y].setBackground(Color.YELLOW);
            break;
            case 2:
                gridButtons[x][y].setBorder(
                        BorderFactory.createLoweredSoftBevelBorder());
                gridButtons[x][y].setBackground(new Color(250,176,2));// ORANGE
            break;
            case 3:
                gridButtons[x][y].setBorder(
                        BorderFactory.createLoweredSoftBevelBorder());
                gridButtons[x][y].setBackground(new Color(218,1,16));// RED
                playSoundEffect("KLoggins");
            break;
        }
    }

    /**
     * Checks if current button contains a mine.
     * @param x the x coordinate for current button
     * @param y the y coordinate for current button
     * @return Returns <code>true</code> if the button contains a mine;
     *         <cod>false</code> otherwise
     */
    private boolean isMine(int x, int y)
    {
        return(mines.contains(new Point(x,y)));
    }

    /**
     * Checks if current game is over
     * @return Returns <code>true</code> if game is over; false otherwise.
     */
    private boolean gameOver()
    {
        if(current.x == 0 && current.y == 0)
        {
            gameState = GameState.SUCCESSFUL_ARRIVAL;
            return true;
        }
        else if(livesCount == 0)
        {
            gameState = GameState.NO_LIVES;
            return true;
        }
        return false;
    }

    /**
     * Action performed when game is over.
     * @param currentState - the state that the game ends in (i.e player uses
     *        all lives or are successful)
     */
    private void gameOverAction(GameState currentState)
    {
        switch (currentState)
        {
            case SUCCESSFUL_ARRIVAL:
                gridButtons[current.x][current.y].setText("X");
                playSoundEffect("Fanfare.wav");
                JOptionPane.showMessageDialog(this,
                        "YOU WIN!" + " Final score: " + scoreCount,"WINNER",
                        JOptionPane.INFORMATION_MESSAGE,new ImageIcon(
                                "assets" + File.separator + "happyFace.png"));

                disableGrid();
                buttonResponse(ButtonCommands.SHOW_MINES);
                buttonResponse(ButtonCommands.SHOW_PATH);
                gridTextField.setEnabled(true);
                diffSelection.setEnabled(true);
                newGame.setText("NEW GAME");
            break;

            case NO_LIVES:
                playSoundEffect("DemomanTaunt.wav");
                JOptionPane.showMessageDialog(this,
                        "You lost all your lives." + " Final score: "
                                + scoreCount,
                        "GAME OVER",JOptionPane.INFORMATION_MESSAGE,
                        new ImageIcon("assets" + File.separator + "sad.png"));

                disableGrid();
                buttonResponse(ButtonCommands.SHOW_MINES);
                buttonResponse(ButtonCommands.SHOW_PATH);
                gridTextField.setEnabled(true);
                diffSelection.setEnabled(true);
                newGame.setText("NEW GAME");
            break;
        }
    }

    private GameState getGameState()
    {
        return gameState;
    }

    // PANELS CREATION
    /**
     * Generate a difficulty depending on user choice
     */
    private double getDifficulty()
    {
        double difficulty;

        if(diffSelection.getSelectedIndex() == 0)
        {
            difficulty = 4;// easy
        }
        else if(diffSelection.getSelectedIndex() == 1)
        {
            difficulty = 2.5;// medium
        }
        else
        {
            difficulty = 2;// hard
        }
        return difficulty;
    }

    /**
     * Sets up difficulty selection box
     */
    private void initDiffSelection()
    {
        diffSelection = new JComboBox<String>(
                new String[]
        {"EASY","MEDIUM","HARD"});
        diffSelection.setToolTipText("WORK HARD...PLAY HARD");
        diffSelection.setBorder(BorderFactory.createTitledBorder("DIFFICULTY"));
        diffSelection.setFont(new Font("Helvetica",Font.PLAIN,12));
        diffSelection.addActionListener(this);
    }

    /**
     * Create a new panel with buttons using the specified grid size.
     */
    private void initMinePanel()
    {
        if(gridPanel != null)
        {
            this.remove(gridPanel);
        }

        RandomWalk walk = new RandomWalk(currentGrid);
        gridPanel = new JPanel();
        gridButtons = new JButton[currentGrid][currentGrid];

        gridPanel.setLayout(new GridLayout(currentGrid,currentGrid,2,2));
        gridPanel.setBorder(BorderFactory.createEtchedBorder());

        for(int p = 0; p < gridButtons.length; p++)
        {
            for(int j = 0; j < gridButtons[p].length; j++)
            {
                gridButtons[p][j] = new JButton();
                gridButtons[p][j]
                        .setBorder(BorderFactory.createRaisedSoftBevelBorder());
                gridButtons[p][j].setFont(new Font("Helvetica",Font.PLAIN,20));
                gridButtons[p][j].addActionListener(this);
                gridButtons[p][j].setHorizontalAlignment(SwingConstants.CENTER);
                gridPanel.add(gridButtons[p][j]);
            }
        }

        alreadyClicked = new ArrayList<Point>();

        current = new Point(currentGrid - 1,currentGrid - 1);
        alreadyClicked.add(current);
        gridButtons[current.x][current.y].setText("X");
        gridButtons[current.x][current.y]
                .setBorder(BorderFactory.createLoweredSoftBevelBorder());
        gridButtons[0][0].setBackground(Color.cyan);

        walk.generateMines(getDifficulty());
        path = walk.getPath();
        mines = walk.getMines();// so we can check if the button just pushed is
                                // a mine

        // add new gridPanel to primary panel & refresh with new content
        add(gridPanel,BorderLayout.CENTER);
        revalidate();
    }

    /**
     * Initialize control panel
     */
    private void initControlPanel()
    {
        gridTextField = new JTextField(Integer.toString(DEFAULT_GRID),5);
        initDiffSelection();
        JPanel controlPanel = new JPanel();
        //        controlPanel.setLayout(new BoxLayout(controlPanel,BoxLayout.X_AXIS));
        gridTextField.setBorder(BorderFactory.createTitledBorder("GRID SIZE"));
        gridTextField.setFont(new Font("Helvetica",Font.PLAIN,13));
        gridTextField.addActionListener(this);

        newGame = new JButton("NEW GAME");
        newGame.setFont(new Font("Helvetica",Font.BOLD,13));
        newGame.addActionListener(this);

        showMines = new JButton("SHOW MINES");
        showMines.setFont(new Font("Helvetica",Font.BOLD,13));
        showMines.addActionListener(this);

        showPath = new JButton("SHOW PATH");
        showPath.setFont(new Font("Helvetica",Font.BOLD,13));
        showPath.addActionListener(this);

        // add component to controlPanel
        controlPanel.add(diffSelection);
        controlPanel.add(gridTextField);
        controlPanel.add(newGame);
        controlPanel.add(showMines);
        controlPanel.add(showPath);

        add(controlPanel,BorderLayout.NORTH);
    }

    /**
     * Initialize game stats panel
     */
    private void initGameStatsPanel()
    {
        JPanel gameStatsPanel = new JPanel();
        gameStatsPanel
                .setLayout(new BoxLayout(gameStatsPanel,BoxLayout.Y_AXIS));
        gameStatsPanel.setPreferredSize(new Dimension(125,100));

        scoreLabel = new JLabel("SCORE: " + scoreCount);
        lifeLabel = new JLabel("LIVES: " + livesCount);

        gameStatsPanel.add(Box.createRigidArea(new Dimension(0,50)));
        gameStatsPanel.add(scoreLabel);
        gameStatsPanel.add(Box.createRigidArea(new Dimension(0,15)));
        gameStatsPanel.add(lifeLabel);
        gameStatsPanel.add(Box.createRigidArea(new Dimension(0,15)));
        gameStatsPanel.add(initKeyPanel());
        gameStatsPanel.add(Box.createVerticalGlue());

        add(gameStatsPanel,BorderLayout.WEST);
    }

    /**
     * Initialize key color panel
     */
    private JPanel initKeyPanel()
    {
        JPanel keyPanel = new JPanel();
        keyPanel.setLayout(new BoxLayout(keyPanel,BoxLayout.Y_AXIS));
        keyPanel.setBorder(BorderFactory.createTitledBorder("KEY"));
        keyPanel.setToolTipText("BE SAFE OUT THERE");

        final JLabel CURRENT_POSITION = new JLabel("CURRENT: X");
        CURRENT_POSITION.setFont(new Font("Helvetica",Font.BOLD,12));
        CURRENT_POSITION.setOpaque(true);

        final JLabel DESTINATION = new JLabel("DESTINATION");
        DESTINATION.setFont(new Font("Helvetica",Font.BOLD,12));
        DESTINATION.setBackground(Color.CYAN);
        DESTINATION.setOpaque(true);

        final JLabel NOMINES = new JLabel("0 MINES NEARBY");
        NOMINES.setFont(new Font("Helvetica",Font.BOLD,12));
        NOMINES.setBackground(Color.GREEN);
        NOMINES.setOpaque(true);

        final JLabel ONE_MINE = new JLabel("1 MINE NEARBY");
        ONE_MINE.setFont(new Font("Helvetica",Font.BOLD,12));
        ONE_MINE.setBackground(Color.YELLOW);
        ONE_MINE.setOpaque(true);

        final JLabel TWO_MINES = new JLabel("2 MINES NEARBY");
        TWO_MINES.setFont(new Font("Helvetica",Font.BOLD,12));
        TWO_MINES.setBackground(Color.ORANGE);
        TWO_MINES.setOpaque(true);

        final JLabel THREE_MINES = new JLabel("3 MINES NEARBY");
        THREE_MINES.setFont(new Font("Helvetica",Font.BOLD,12));
        THREE_MINES.setBackground(new Color(218,1,16));
        THREE_MINES.setOpaque(true);

        keyPanel.add(CURRENT_POSITION);
        keyPanel.add(NOMINES);
        keyPanel.add(ONE_MINE);
        keyPanel.add(TWO_MINES);
        keyPanel.add(THREE_MINES);
        keyPanel.add(DESTINATION);

        return keyPanel;
    }

    /**
     * Action performed when a mine is clicked on.
     * @param p The X coordinate of the mine.
     * @param j The Y coordinate of the mine.
     */
    private void mineAction(int p, int j)
    {
        scoreLabel.setText("SCORE: " + (scoreCount - LIFE_DEDUCTION));
        lifeLabel.setText("LIVES: " + --livesCount);
        gridButtons[p][j]
                .setBorder(BorderFactory.createLoweredSoftBevelBorder());
        gridButtons[p][j].setBackground(Color.BLACK);
        gridButtons[p][j].removeActionListener(this);
        gridButtons[p][j].setIcon(EXPLOSIONS);
        playSoundEffect("TCrews.wav");
    }

    /**
     * Action performed pressing "NEW GAME" or hitting ENTER after inputting
     * grid size
     */
    private void newGame()
    {
        if(diffSelection.getSelectedIndex() == 2)
            livesCount = 3;
        else
            livesCount = 5;

        scoreCount = 500;
        initMinePanel();
        newGame.setText("QUIT");
        showMines.setText("SHOW MINES");
        showPath.setText("SHOW PATH");
        scoreLabel.setText("SCORE: " + (scoreCount));
        lifeLabel.setText("LIVES: " + (livesCount));
        gridTextField.setEnabled(false);
        diffSelection.setEnabled(false);
    }

    /**
     * Reacts to a button command
     * @param command - the button pressed
     */
    private void buttonResponse(ButtonCommands command)
    {
        for(int p = 0; p < gridButtons.length; p++)
        {
            for(int j = 0; j < gridButtons[p].length; j++)
            {
                Point temp = new Point(p,j);
                switch (command)
                {
                    case SHOW_MINES:
                        newGame.setText("NEW GAME");
                        showMines.setText("HIDE MINES");
                        if(mines.contains(temp))
                            gridButtons[p][j].setBackground(Color.BLACK);
                    break;

                    case HIDE_MINES:
                        showMines.setText("SHOW MINES");
                        if(mines.contains(temp))
                            gridButtons[p][j].setBackground(getBackground());
                    break;

                    case SHOW_PATH:
                        gridTextField.setEnabled(true);
                        diffSelection.setEnabled(true);
                        showPath.setText("HIDE PATH");
                        if(path.contains(temp))
                            gridButtons[p][j].setBackground(Color.BLUE);
                    break;

                    case HIDE_PATH:
                        showPath.setText("SHOW PATH");
                        if(path.contains(temp))
                            gridButtons[p][j].setBackground(getBackground());
                    break;
                }
            }
        }
        gridButtons[0][0].setBackground(Color.cyan);
    }

    /**
     * Disables game board
     */
    private void disableGrid()
    {
        for(int p = 0; p < gridButtons.length; p++)
        {
            for(int j = 0; j < gridButtons[p].length; j++)
            {
                gridButtons[p][j].setEnabled(false);
            }
        }
    }

    /**
     * Performs action when timer event fires.
     */
    private class TimerActionListener implements ActionListener
    {

        private boolean keyColor = true;

        public void actionPerformed(ActionEvent evt)
        {
            if(keyColor)
            {
                changeColor(current.x,current.y);
                gridButtons[current.x][current.y].setText("X");
                keyColor = false;
            }
            else
            {
                gridButtons[current.x][current.y]
                        .setBackground(Color.LIGHT_GRAY);
                gridButtons[current.x][current.y].setText("");
                keyColor = true;
            }
        }
    }

    /**
     * Create an animation thread that runs periodically
     */
    private void startAnimation()
    {
        TimerActionListener taskPerformer = new TimerActionListener();
        new Timer(DELAY,taskPerformer).start();
    }

    // ALL PUBLIC CLASS METHODS
    /**
     * Plays an audio clip
     * @param soundFile The sound file to be played.
     */
    public void playSoundEffect(String soundFile)
    {
        if(clip != null)
        {
            clip.stop();
        }

        try
        {
            clip = java.applet.Applet.newAudioClip(new java.net.URL("file",
                    "localhost","assets" + File.separator + soundFile));
            clip.play();
        }
        catch(MalformedURLException badURL)
        {
            System.err.println(badURL.getMessage());
        }
    }

    /**
     * Checks to see if the grid passed in is a valid.
     * @return <code>true</code> if valid; <code>false</code> otherwise and
     *         displays a JOptionPane information dialog.
     */
    public boolean validGrid()
    {
        try
        {
            if(getGrid() >= 4 && getGrid() <= 25)
                return true;
            else
            {
                JOptionPane.showMessageDialog(this,
                        "Please enter a grid size between 4-25.","INVALID GRID",
                        JOptionPane.PLAIN_MESSAGE);
            }
        }
        catch(NumberFormatException e)
        {
            JOptionPane.showMessageDialog(this,
                    "Please enter a grid size between 4-25.","INVALID GRID",
                    JOptionPane.PLAIN_MESSAGE);
        }
        return false;
    }

    private int getGrid() throws NumberFormatException
    {
        return currentGrid = Integer.parseInt(gridTextField.getText());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
        // gridSizeText field event
        if(event.getSource().equals(gridTextField))
        {
            if(validGrid())
                newGame();
        }
        // new game event
        else if(event.getSource().equals(newGame))
        {
            JButton sourceButton = (JButton) event.getSource();
            if(sourceButton.getText().equalsIgnoreCase("new game"))
            {
                if(validGrid())
                    newGame();
            }
            else
            {
                disableGrid();
                gridTextField.setEnabled(true);
                diffSelection.setEnabled(true);
                buttonResponse(ButtonCommands.SHOW_PATH);
                buttonResponse(ButtonCommands.SHOW_MINES);
            }
        }
        // show/hide path events
        else if(event.getSource().equals(showPath))
        {
            JButton sourceButton = (JButton) event.getSource();
            if(sourceButton.getText().equalsIgnoreCase("show path"))
            {
                disableGrid();
                gridTextField.setEnabled(true);
                diffSelection.setEnabled(true);
                buttonResponse(ButtonCommands.SHOW_PATH);
                newGame.setText("NEW GAME");
            }
            else
                buttonResponse(ButtonCommands.HIDE_PATH);
        }
        // show/hide mines events
        else if(event.getSource().equals(showMines))
        {
            JButton sourceButton = (JButton) event.getSource();
            if(sourceButton.getText().equalsIgnoreCase("show mines"))
            {
                disableGrid();
                gridTextField.setEnabled(true);
                diffSelection.setEnabled(true);
                buttonResponse(ButtonCommands.SHOW_MINES);
            }
            else
                buttonResponse(ButtonCommands.HIDE_MINES);
        }
        // clicking on grid
        else
        {
            for(int p = 0; p < gridButtons.length; p++)
            {
                for(int j = 0; j < gridButtons[p].length; j++)
                {
                    if(event.getSource().equals(gridButtons[p][j]))
                    {
                        if(!validMove(p,j))
                        {
                            playSoundEffect("TROLL2.wav");
                        }
                        else
                        {
                            if(isMine(p,j))
                                mineAction(p,j);// needs (p,j) to know which
                                                // tile to color
                            else
                            {
                                Point clicked = new Point(p,j);
                                if(!alreadyClicked.contains(clicked))
                                {
                                    scoreLabel.setText("SCORE: "
                                            + (scoreCount += SCORE_ADDITIVE));
                                    alreadyClicked.add(clicked);
                                }
                                current = new Point(p,j);
                            }
                            if(gameOver())
                                gameOverAction(getGameState());
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("MineWalker, Texas Ranger");
        frame.setPreferredSize(new Dimension(750,600));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.add(new MineWalkerV1());

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
