import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <code>ControlPanel</code> is the control class for MineWalker.
 *
 * @author Prince Kannah
 */
class ControlPanel extends JPanel implements ActionListener {
    private JButton showPathButton, showMinesButton, newGameButton;
    private JTextField gridInputField;
    private JComboBox<String> diffSelection;
    private int gridSize;
    private GridPanel gp;
    private Timer timer;

    /**
     * CONSTRUCTOR: sets up the control panel and and all other sub-panels.
     */
    ControlPanel() {
        gp = new GridPanel();
        setLayout(new BorderLayout());
        setFont(new Font("Helvetica", Font.BOLD, 13));
        initGridField();
        initDiffSelection();

        add(initControls(), BorderLayout.NORTH);
        add(gp.setupGrid(getGridSize(), getDifficulty()), BorderLayout.CENTER);
        add(new GameStatsPanel(), BorderLayout.WEST);

        newGameButton.setText("QUIT");
        gridInputField.setEnabled(false);
        diffSelection.setEnabled(false);
        startAnimation();
    }

    /**
     * @return sets up and return the JPanel pane
     */
    private JPanel initControls() {
        JPanel panel = new JPanel();
        newGameButton = new JButton("NEW GAME");
        newGameButton.addActionListener(this);

        showMinesButton = new JButton("SHOW MINES");
        showMinesButton.addActionListener(this);

        showPathButton = new JButton("SHOW PATH");
        showPathButton.addActionListener(this);

        panel.add(diffSelection);
        panel.add(gridInputField);
        panel.add(newGameButton);
        panel.add(showMinesButton);
        panel.add(showPathButton);
        return panel;
    }

    /**
     * Sets up the grid input text field
     */
    private void initGridField() {
        gridInputField = new JTextField(Integer.toString(GridPanel.DEFAULT_GRID), 8);
        gridInputField.setBorder(BorderFactory.createTitledBorder("GRID SIZE"));
        gridInputField.setFont(new Font("Helvetica", Font.PLAIN, 13));
        gridInputField.addActionListener(this);
    }

    /**
     * Sets up the difficulty selection drop down
     */
    private void initDiffSelection() {
        diffSelection = new JComboBox<>(
                new String[]
                        {"EASY", "MEDIUM", "HARD"});
        diffSelection.setToolTipText("WORK HARD...PLAY HARD");
        diffSelection.setBorder(BorderFactory.createTitledBorder("DIFFICULTY"));
        diffSelection.setFont(new Font("Helvetica", Font.PLAIN, 12));
        diffSelection.addActionListener(this);
    }

    /**
     * Game action taken when a new game is started
     */
    private void newGame() {
        remove(gp.getGrid());
        if (diffSelection.getSelectedIndex() == 2)
            GameStatsPanel.setLifeCount(3);
        else
            GameStatsPanel.setLifeCount(5);

        GameStatsPanel.resetScore();
        add(gp.setupGrid(getGridSize(), getDifficulty()));
        revalidate();

        newGameButton.setText("QUIT");
        showMinesButton.setText("SHOW MINES");
        showPathButton.setText("SHOW PATH");
        gridInputField.setEnabled(false);
        diffSelection.setEnabled(false);
        timer.start();
    }

    /**
     * Sets the difficulty based on JCombobox selection
     *
     * @return the difficulty
     */
    private double getDifficulty() {
        double difficulty;
        if (diffSelection.getSelectedIndex() == 0)
            difficulty = 4; //easy
        else if (diffSelection.getSelectedIndex() == 1)
            difficulty = 2.5; //medium
        else
            difficulty = 2;//hard

        return difficulty;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(gridInputField)) {
            if (validGrid()) newGame();
        } else if (e.getSource().equals(newGameButton)) {
            JButton source = (JButton) e.getSource();
            if (source.getText().equalsIgnoreCase("new game")) {
                if (validGrid())
                    if (validGrid()) newGame();
            } else {
                gp.disableGrid();
                gridInputField.setEnabled(true);
                diffSelection.setEnabled(true);
                newGameButton.setText("NEW GAME");
                showPathButton.setText("HIDE PATH");
                showMinesButton.setText("HIDE MINES");
                gp.buttonResponse(ButtonCommands.SHOW_PATH);
                gp.buttonResponse(ButtonCommands.SHOW_MINES);
            }
        }
        // show/hide path events
        else if (e.getSource().equals(showPathButton)) {
            JButton sourceButton = (JButton) e.getSource();
            if (sourceButton.getText().equalsIgnoreCase("show path")) {
                gp.disableGrid();
                gridInputField.setEnabled(true);
                diffSelection.setEnabled(true);
                gp.buttonResponse(ButtonCommands.SHOW_PATH);
                newGameButton.setText("NEW GAME");
                showPathButton.setText("HIDE PATH");
            } else {
                gp.buttonResponse(ButtonCommands.HIDE_PATH);
                showPathButton.setText("SHOW PATH");
            }
        }
        // show/hide mines events
        else if (e.getSource().equals(showMinesButton)) {
            JButton sourceButton = (JButton) e.getSource();
            if (sourceButton.getText().equalsIgnoreCase("show mines")) {
                gp.disableGrid();
                gridInputField.setEnabled(true);
                diffSelection.setEnabled(true);
                gp.buttonResponse(ButtonCommands.SHOW_MINES);
                newGameButton.setText("NEW GAME");
                showMinesButton.setText("HIDE MINES");
            } else {
                gp.buttonResponse(ButtonCommands.HIDE_MINES);
                showMinesButton.setText("SHOW MINES");
            }
        }
    }

    /**
     * Gets the grid from the grid input filed
     *
     * @return the input from the grid size input field.
     * @throws NumberFormatException
     */
    private int getGridSize() throws NumberFormatException {
        return gridSize = Integer.parseInt(gridInputField.getText());
    }

    /**
     * Checks if the input grid is valid.
     *
     * @return true if the grid value is valid; false otherwise
     */
    private boolean validGrid() {
        try {
            getGridSize();
            if (gridSize >= 4 && gridSize <= 25)
                return true;
            else {
                JOptionPane.showMessageDialog(this,
                        "Please enter a gridSize size between 4-25.", "INVALID GRID",
                        JOptionPane.PLAIN_MESSAGE);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "Please enter a gridSize size between 4-25.", "INVALID GRID",
                    JOptionPane.PLAIN_MESSAGE);
        }
        return false;
    }

    /**
     * Timer action listener class.
     */
    private class TimerActionListener implements ActionListener {
        /**
         * Game action taken when game is over
         * @param evt the event that triggers the timer
         */
        public void actionPerformed(ActionEvent evt) {
            if (gp.gameOver()) {
                newGameButton.setText("NEW GAME");
                showMinesButton.setText("HIDE MINES");
                showPathButton.setText("HIDE PATH");
                gridInputField.setEnabled(true);
                diffSelection.setEnabled(true);
                timer.stop();
            }
        }
    }

    /**
     * Starts the animation timer
     */
    private void startAnimation() {
        TimerActionListener taskPerformer = new TimerActionListener();
        int DELAY = 100;
        timer = new Timer(DELAY, taskPerformer);
        timer.start();
    }
}
