import javax.swing.*;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;

import static java.awt.Color.*;

/**
 * <code>GridPanel</code> JPanel object use for the grid in MineWalker.
 * @author Prince Kannah
 */
class GridPanel implements ActionListener {
    static final int DEFAULT_GRID = 10;
    private final Color ORANGE = new Color(250, 176, 2), RED = new Color(218, 1, 16);
    private ArrayList<Point> alreadyClicked, path, mines;
    private Point current;
    private JButton[][] buttons;
    private JPanel grid;
    private Color defaultColor;
    private GameState state;

    private AudioClip clip;
    private final ImageIcon EXPLOSIONS = new ImageIcon("assets" + File.separator + "Bayhem.gif");

    /**
     * Gets the JPanel pane that contains the mine grid.
     * @return the mine grid panel.
     */
    JPanel getGrid() {
        return grid;
    }

    /**
     * Disables the entire grade
     */
    void disableGrid() {
        for (int p = 0; p < buttons.length; p++) {
            for (int j = 0; j < buttons[p].length; j++) {
                buttons[p][j].setEnabled(false);
            }
        }
    }

    /**
     *
     * @param gridSize the size of the mine grid
     * @param difficulty the number use to determine the number of mines placed on the grid
     * @return the JPanel pane with
     */
    JPanel setupGrid(int gridSize, double difficulty) {
        if (grid != null)
            grid.remove(grid);
        grid = new JPanel();
        RandomWalk walk = new RandomWalk(gridSize);
        buttons = new JButton[gridSize][gridSize];

        grid.setLayout(new GridLayout(gridSize, gridSize, 2, 2));
        grid.setBorder(BorderFactory.createEtchedBorder());

        for (int i = 0; i < buttons.length; i++) {
            for (int j = 0; j < buttons[i].length; j++) {
                buttons[i][j] = new JButton();
                buttons[i][j].setBorder(BorderFactory.createRaisedSoftBevelBorder());
                buttons[i][j].setFont(new Font("Helvetica", Font.PLAIN, 20));
                buttons[i][j].addActionListener(this);
                buttons[i][j].setHorizontalAlignment(SwingConstants.CENTER);
                grid.add(buttons[i][j]);
            }
        }
        defaultColor = buttons[0][0].getBackground();
        buttons[0][0].setBackground(CYAN);
        alreadyClicked = new ArrayList<>();
        current = new Point(gridSize - 1, gridSize - 1);
        alreadyClicked.add(current);

        walk.generateMines(difficulty);
        path = walk.getPath();
        mines = walk.getMines();
        grid.revalidate();
        startAnimation();
        return grid;
    }

    /**
     * Plays the specified audio file
     * @param audioFile the audio file to play
     */
    private void playSoundEffect(String audioFile) {
        if (clip != null) {
            clip.stop();
        }

        try {
            clip = java.applet.Applet.newAudioClip(new java.net.URL("file",
                    "localhost", "assets" + File.separator + audioFile));
            clip.play();
        } catch (MalformedURLException badURL) {
            System.err.println(badURL.getMessage());
        }
    }

    /**
     * Game action taken when a tile within a mine is clicked
     * @param x the x coordinate of the tile
     * @param y the y coordinate of the tile
     */
    private void mineAction(int x, int y) {
        GameStatsPanel.updateLifeCount();
        GameStatsPanel.decrementScore(GameStatsPanel.LIFE_DEDUCTION);
        buttons[x][y].setBorder(BorderFactory.createLoweredSoftBevelBorder());
        buttons[x][y].setBackground(BLACK);
        buttons[x][y].removeActionListener(this);
        buttons[x][y].setIcon(EXPLOSIONS);
        playSoundEffect("TCrews.wav");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < buttons.length; i++) {
            for (int j = 0; j < buttons[i].length; j++) {
                if (e.getSource().equals(buttons[i][j])) {
                    if (!validMove(i, j)) playSoundEffect("TROLL2.wav");
                    else {
                        if (isMine(i, j)) mineAction(i, j);
                        else {
                            Point clicked = new Point(i, j);
                            if (!alreadyClicked.contains(clicked)) {
                                GameStatsPanel.updateScoreCount(GameStatsPanel.SCORE_ADDITIVE);
                                alreadyClicked.add(clicked);
                            }
                            current = new Point(i, j);
                        }
                        if (gameOver()) gameOverAction(state);
                    }
                }
            }
        }
    }

    /**
     * Actions taken when the game ends.
     * @param currentState the state the game ends in
     */
    private void gameOverAction(GameState currentState) {
        switch (currentState) {
            case SUCCESSFUL_ARRIVAL:
                buttons[current.x][current.y].setText("X");
                playSoundEffect("Fanfare.wav");
                JOptionPane.showMessageDialog(grid,
                        "YOU WIN!" + " Final score: " + GameStatsPanel.getScore(), "WINNER",
                        JOptionPane.INFORMATION_MESSAGE, new ImageIcon(
                                "assets" + File.separator + "happyFace.png"));

                disableGrid();
                buttonResponse(ButtonCommands.SHOW_MINES);
                buttonResponse(ButtonCommands.SHOW_PATH);
                break;

            case NO_LIVES:
                playSoundEffect("DemomanTaunt.wav");
                JOptionPane.showMessageDialog(grid,
                        "You lost all your lives." + " Final score: "
                                + GameStatsPanel.getScore(),
                        "GAME OVER", JOptionPane.INFORMATION_MESSAGE,
                        new ImageIcon("assets" + File.separator + "sad.png"));

                disableGrid();
                buttonResponse(ButtonCommands.SHOW_MINES);
                buttonResponse(ButtonCommands.SHOW_PATH);
                break;
        }
    }

    /**
     * Checks whether the move made is valid
     * @param x the x coordinate
     * @param y the y coordinate
     * @return true if the move is valid; false otherwise.
     */
    private boolean validMove(int x, int y) {
        if (x == current.x && y == current.y - 1)
            return true;
        else if (x == current.x && y == current.y + 1)
            return true;
        else if (x == current.x + 1 && y == current.y)
            return true;
        else if (x == current.x - 1 && y == current.y)
            return true;

        return false;
    }

    /**
     * Checks whether the game is over
     * @return true if the game is over; false otherwise
     */
    boolean gameOver() {
        if (current.x == 0 && current.y == 0) {
            state = GameState.SUCCESSFUL_ARRIVAL;
            return true;
        } else if (GameStatsPanel.getLivesCount() == 0) {
            state = GameState.NO_LIVES;
            return true;
        }
        return false;
    }

    /**
     * Game action taken when a specific button is pressed
     * @param command the button that was pushed
     */
    void buttonResponse(ButtonCommands command) {
        for (int p = 0; p < buttons.length; p++) {
            for (int j = 0; j < buttons[p].length; j++) {
                Point temp = new Point(p, j);
                switch (command) {
                    case SHOW_MINES:
                        if (mines.contains(temp))
                            buttons[p][j].setBackground(BLACK);
                        break;
                    case HIDE_MINES:
                        if (mines.contains(temp))
                            buttons[p][j].setBackground(defaultColor);
                        break;
                    case SHOW_PATH:
                        if (path.contains(temp))
                            buttons[p][j].setBackground(BLUE);
                        break;
                    case HIDE_PATH:
                        if (path.contains(temp))
                            buttons[p][j].setBackground(defaultColor);
                        break;
                }
            }
        }
        buttons[0][0].setBackground(CYAN);
    }

    /**
     * Checks whether the specified coordinates is a mine
     * @param x the x coordinate
     * @param y the y coordinate
     * @return true if the specified point is a mine; false otherwise
     */
    private boolean isMine(int x, int y) {
        return mines.contains(new Point(x, y));
    }

    /**
     * Starts the animation timer
     */
    private void startAnimation() {
        TimerActionListener taskPerformer = new TimerActionListener();
        int DELAY = 100;
        new Timer(DELAY, taskPerformer).start();
    }

    /**
     * Gets the total number of mines around the specified point
     * @param x the x coordinate
     * @param y the y coordinate
     * @return the total number of mines around the specified point
     */
    private int getNumMines(int x, int y) {
        int numMines = 0;

        if (mines.contains(new Point(x, y - 1))) numMines++;
        if (mines.contains(new Point(x, y + 1))) numMines++;
        if (mines.contains(new Point(x - 1, y))) numMines++;
        if (mines.contains(new Point(x + 1, y))) numMines++;

        return numMines;
    }

    /**
     * Changes the color of the current tile based on the number of mines
     * @param x the x coordinate of the tile
     * @param y the y coordinate of the tile
     */
    private void changeColor(int x, int y) {
        int numMines = getNumMines(x, y);
        switch (numMines) {
            case 0:
                buttons[x][y].setBorder(
                        BorderFactory.createLoweredSoftBevelBorder());
                buttons[x][y].setBackground(GREEN);
                break;
            case 1:
                buttons[x][y].setBorder(
                        BorderFactory.createLoweredSoftBevelBorder());
                buttons[x][y].setBackground(YELLOW);
                break;
            case 2:
                buttons[x][y].setBorder(
                        BorderFactory.createLoweredSoftBevelBorder());
                buttons[x][y].setBackground(this.ORANGE);
                break;
            case 3:
                buttons[x][y].setBorder(
                        BorderFactory.createLoweredSoftBevelBorder());
                buttons[x][y].setBackground(this.RED);
                playSoundEffect("KLoggins.wav");
                break;
        }
    }

    /**
     * Timer action listener class. Use in the blinking tile animation
     */
    private class TimerActionListener implements ActionListener {
        private boolean keyColor = true;

        /**
         * The action performed when the timer event is fired
         * @param evt the event
         */
        public void actionPerformed(ActionEvent evt) {
            if (keyColor) {
                changeColor(current.x, current.y);
                buttons[current.x][current.y].setText("X");
                keyColor = false;
            } else {
                buttons[current.x][current.y]
                        .setBackground(Color.LIGHT_GRAY);
                buttons[current.x][current.y].setText("");
                keyColor = true;
            }
        }
    }
}

