import javax.swing.*;
import java.awt.*;

public class MineWalker
{
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("MineWalker, Texas Ranger");
        frame.setPreferredSize(new Dimension(750,600));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.add(new ControlPanel());

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
