import javax.swing.*;
import java.awt.*;

/**
 * <code>GameStatsPanel</code> is use to display game stats and information for MineWalker
 *
 * @author Prince Kannah
 */
class GameStatsPanel extends JPanel {
    /**
     * Points lost per mine
     */
    static final int LIFE_DEDUCTION = 5;

    /**
     * Points added to score for each valid move.
     */
    static final int SCORE_ADDITIVE = 10;
    private static int scoreCount = 500;// initial score
    private static int livesCount = 5;// initial number of lives
    private static JLabel score, life;

    /**
     * CONSTRUCTOR: sets up the game stats JPanel pane.
     */
    GameStatsPanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setPreferredSize(new Dimension(125, 100));

        score = new JLabel("SCORE: " + scoreCount);
        life = new JLabel("LIVES: " + livesCount);

        add(Box.createRigidArea(new Dimension(0, 50)));
        add(score);
        add(Box.createRigidArea(new Dimension(0, 15)));
        add(life);
        add(Box.createRigidArea(new Dimension(0, 15)));
        add(initKeyPanel());
        add(Box.createVerticalGlue());
    }

    /**
     * Adds the specified count to the current score
     *
     * @param count the value to add to score
     */
    static void updateScoreCount(int count) {
        scoreCount += count;
        score.setText("SCORE: " + scoreCount);
    }

    /**
     * @return the current score
     */
    static int getScore() {
        return scoreCount;
    }

    /**
     * Decrement the current score by the specified point
     *
     * @param point the point to decrement the score by
     */
    static void decrementScore(int point) {
        scoreCount -= point;
        score.setText("SCORE: " + scoreCount);
    }

    /**
     * Resets the score to the default (500)
     */
    static void resetScore() {
        scoreCount = 500;
        score.setText("SCORE: " + scoreCount);
    }

    /**
     * Decrements the lives count by one (1)
     */
    static void updateLifeCount() {
        life.setText("LIVES: " + (--livesCount));
    }

    /**
     * Updates the life count to the specified value
     *
     * @param count the values to set the life to
     */
    static void setLifeCount(int count) {
        livesCount = count;
        life.setText("LIVES: " + livesCount);
    }

    /**
     * @return the current life count
     */
    static int getLivesCount() {
        return livesCount;
    }

    /**
     * @return sets up and return the game stats JPanel
     */
    private JPanel initKeyPanel() {
        JPanel keyPanel = new JPanel();
        keyPanel.setLayout(new BoxLayout(keyPanel, BoxLayout.Y_AXIS));
        keyPanel.setBorder(BorderFactory.createTitledBorder("KEY"));
        keyPanel.setToolTipText("BE SAFE OUT THERE");

        final JLabel CURRENT_POSITION = new JLabel("CURRENT: X");
        CURRENT_POSITION.setFont(new Font("Helvetica", Font.BOLD, 12));
        CURRENT_POSITION.setOpaque(true);

        final JLabel DESTINATION = new JLabel("DESTINATION");
        DESTINATION.setFont(new Font("Helvetica", Font.BOLD, 12));
        DESTINATION.setBackground(Color.CYAN);
        DESTINATION.setOpaque(true);

        final JLabel NO_MINES = new JLabel("0 MINES NEARBY");
        NO_MINES.setFont(new Font("Helvetica", Font.BOLD, 12));
        NO_MINES.setBackground(Color.GREEN);
        NO_MINES.setOpaque(true);

        final JLabel ONE_MINE = new JLabel("1 MINE NEARBY");
        ONE_MINE.setFont(new Font("Helvetica", Font.BOLD, 12));
        ONE_MINE.setBackground(Color.YELLOW);
        ONE_MINE.setOpaque(true);

        final JLabel TWO_MINES = new JLabel("2 MINES NEARBY");
        TWO_MINES.setFont(new Font("Helvetica", Font.BOLD, 12));
        TWO_MINES.setBackground(Color.ORANGE);
        TWO_MINES.setOpaque(true);

        final JLabel THREE_MINES = new JLabel("3 MINES NEARBY");
        THREE_MINES.setFont(new Font("Helvetica", Font.BOLD, 12));
        THREE_MINES.setBackground(new Color(218, 1, 16));
        THREE_MINES.setOpaque(true);

        keyPanel.add(CURRENT_POSITION);
        keyPanel.add(NO_MINES);
        keyPanel.add(ONE_MINE);
        keyPanel.add(TWO_MINES);
        keyPanel.add(THREE_MINES);
        keyPanel.add(DESTINATION);

        return keyPanel;
    }
}
