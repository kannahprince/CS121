import java.util.Random;
import java.util.ArrayList;
import java.awt.Point;

/**
 * This represents a random walk within a specified grid size. A seed can be use
 * for debugging purposes.
 * @author Prince Kannah
 */

public class RandomWalk
{

    //instance variables for RandomWalk:	
    private int gridSize;
    private int direction;//use to determine if we go right or down
    private Random rand;
    private boolean done;

    private Point start;
    private ArrayList<Point> path;
    private ArrayList<Point> mines;

    /**
     * Constructor: Creates a random walk with the specified grid size; takes no
     * seed.
     * @param gridSize size of grid
     */
    public RandomWalk(int gridSize)
    {
        this.gridSize = gridSize;
        rand = new Random();
        done = false;

        start = new Point(this.gridSize - 1,this.gridSize - 1);
        mines = new ArrayList<Point>();
        path = new ArrayList<Point>();

        path.add(start);
    }

    /**
     * Constructor: Create a random walk with the specified grid size and seed.
     * @param gridSize size of grid.
     * @param seed seed value
     */
    public RandomWalk(int gridSize, long seed)
    {
        this.gridSize = gridSize;
        rand = new Random(seed);
        done = false;

        start = new Point(this.gridSize - 1,this.gridSize - 1);
        path = new ArrayList<Point>();
        path.add(start);
    }

    /**
     * Takes a single step on the grid
     */
    public void step()
    {
        direction = rand.nextInt(2);// if 0 go right otherwise go up
        Point current = new Point(path.get(path.size() - 1).x,
                path.get(path.size() - 1).y);

        if(direction == 0 && current.x != 0)
        {
            current.x -= 1;
            path.add(new Point(current.x,current.y));
        }

        if(direction == 1 && current.y != 0)
        {
            current.y -= 1;
            path.add(new Point(current.x,current.y));
        }

        if(current.x == 0 && current.y == 0)
        {
            done = true;
        }
    }

    /**
     * Creates the walk in one call by using the step() method
     */
    public void createWalk()
    {
        while (!isDone())
            step();
    }

    /**
     * @return Returns the current value of the done variable.Does not check to
     *         see if walk is actually complete.
     */
    public boolean isDone()
    {
        return done;
    }

    /**
     * @return The path of the walk.
     */
    public ArrayList<Point> getPath()
    {
        return path;
    }

    /**
     * Gets the array containing the mines that have been placed around the
     * path.
     * @return An <code>ArrayList<Point></code> containing the mines.
     */
    public ArrayList<Point> getMines()
    {
        return mines;
    }

    /**
     * Generate mines around
     * @param difficulty
     */
    public void generateMines(double difficulty)
    {
        createWalk();
        mines.clear();

        for(int p = 0; p < ((gridSize * gridSize) - path.size())
                / difficulty; p++)
        {
            int randX = rand.nextInt(gridSize);
            int randY = rand.nextInt(gridSize);
            Point temp = new Point(randX,randY);

            while (mines.contains(temp) || path.contains(temp))
            {
                randX = rand.nextInt(gridSize);
                randY = rand.nextInt(gridSize);
                temp = new Point(randX,randY);
            }
            mines.add(temp);
        }
    }

    /**
     * @return Return string representation of the walk.
     */
    public String toString()
    {
        String st = "";
        for(Point p : path)
        {
            st += "[" + p.x + "," + p.y + "]";
        }
        return st;
    }
}
