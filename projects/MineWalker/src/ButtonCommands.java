/**
 * An enum use to represent the different button actions.
 */
enum ButtonCommands {
    SHOW_MINES, HIDE_MINES, SHOW_PATH, HIDE_PATH
}
