/**
 * An enum use to represent the different state the game end in.
 */
enum GameState {
    NO_LIVES, SUCCESSFUL_ARRIVAL //player makes it to (0,0)
}
