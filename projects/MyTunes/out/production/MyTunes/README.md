# PlayList.java
  * Add overloaded constructor

    ```java
    public PlayList(File fileName)
    ```
  * Implement PlayListInterface
  
    ```java
    public PlayList implements PlayListInterface {
    ```
  
  * Use MyTunesPlayList in the MyTunesGUI to get access to all the play methods.

# Song.java
  * Extend PlayableSong

  ```java
  public Song extends PlayableSong {
  ```
  
  * Comment out or delete old play() method.

  * Add super(fileName) as the first line of your constructor.
  
    ```java
    public Song(String title, String artist, int playTime, String fileName)
    {
          super(fileName);
          this.title = title;
          this.artist = artist;
          this.playTime = playTime;
          this.fileName = fileName;
    }