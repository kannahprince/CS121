import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by princekannah on 4/7/16.
 * Working project: Jukebox.
 */
@SuppressWarnings("WeakerAccess")
public class PlayList implements MyTunesPlayListInterface {
    private ArrayList<Song> songList;
    private Song playing;
    private String name;
    private int playCount;

    public PlayList(String name) {
        this.name = name;
        songList = new ArrayList<>();
        playing = null;
        playCount = 0;
    }

    public PlayList(File file) {
        playCount = 0;
        songList = new ArrayList<>();
        loadPlayList(file);
    }

    public void addSong(Song s) {
        songList.add(s);
    }

    public Song getSong(int i) {
        if (i >= 0 && i < songList.size())
            return songList.get(i);
        return null;
    }

    public void playSong(int i) {
        stop();
        if (i >= 0 && i < getNumSongs()) {
            songList.get(i).play();
            playing = songList.get(i);
        }
    }

    @Override
    public void playSong(Song song) {
        stop();
        if (song != null) {
            playing = song;
            playing.play();
        }
    }

    @Override
    public void stop() {
        if (playing != null) {
            playing.stop();
        }
    }

    public String getInfo() {
        if (getNumSongs() == 0)
            return "There are no songs.";
        StringBuilder sb = new StringBuilder();
        double average = (double) getTotalPlayTime() / songList.size();
        sb.append("The average play time is: " + String.format("%.2f", average) + " seconds\n");
        sb.append("The shortest song is: " + findShortest() + "\n");
        sb.append("The longest song is: " + findLongest() + "\n");
        sb.append("Total play time: " + getTotalPlayTime() + "\n");

        return sb.toString();
    }

    private Song findLongest() {
        Song temp = new Song("", "", Integer.MIN_VALUE, "");
        for (Song s : songList) {
            if (temp.getPlayTime() < s.getPlayTime())
                temp = s;
        }
        return temp;
    }

    private Song findShortest() {
        Song temp = new Song("", "", Integer.MAX_VALUE, "");
        for (Song s : songList) {
            if (s.getPlayTime() < temp.getPlayTime())
                temp = s;
        }
        return temp;
    }

    public Song getPlaying() {
        return playing;
    }

    public Song removeSong(int id) {
        if (id < songList.size() && id >= 0)
            songList.remove(id);
        return null;
    }

    public int getNumSongs() {
        return songList.size();
    }

    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getPlayCount() {
        return playCount;
    }

    public int getTotalPlayTime() {
        int total = 0;
        for (Song s : songList) {
            total += s.getPlayTime();
        }
        return total;
    }

    private String formatPlayTime() {
        StringBuilder builder = new StringBuilder(4);
        DecimalFormat format = new DecimalFormat("##:00");
        int toMinutes = ((getTotalPlayTime() % 3600) / 60);
        int toSeconds = (getTotalPlayTime() % 60);
        builder.append(format.format(toMinutes));
        builder.append(format.format(toSeconds));
        return builder.toString().substring(0, 5);
    }

    public String getPlayTimeString(){
        return formatPlayTime();
    }
    public ArrayList<Song> getSongList() {
        return songList;
    }

    @Override
    public Song[] getSongArray() {
        Song[] copy = new Song[songList.size()];

        for (int i = 0; i < copy.length; i++) {
            copy[i] = songList.get(i);
        }
        return copy;
    }

    @Override
    public int moveUp(int index) {
        if (index == 0) {
            Song first = getSong(index);
            for (int i = 0; i < getNumSongs(); i++) {
                songList.set(i, getSong(i + 1));
            }
            songList.set(getNumSongs() - 1, first);
            return getNumSongs() - 1;
        } else {
            Song temp = songList.get(index - 1);
            songList.set(index - 1, getSong(index));
            songList.set(index, temp);
        }
        return (index - 1);
    }

    @Override
    public int moveDown(int index) {
        if (index == getNumSongs() - 1) {
            Song last = getSong(index);
            for (int i = getNumSongs() - 1; i >= 0; i--) {
                songList.set(i, getSong(i - 1));
            }
            songList.set(0, last);
            return 0;
        } else {
            Song next = getSong(index + 1);
            songList.set(index + 1, getSong(index));
            songList.set(index, next);
        }
        return (index + 1);
    }

    @Override
    public Song[][] getMusicSquare() {
        int size = (int) Math.ceil(Math.sqrt(getNumSongs()));
        Song[][] copy = new Song[size][size];
        for (int i = 0; i < copy.length; i++) {
            for (int j = 0; j < copy[i].length; j++) {
                copy[i][j] = songList.get((i * size + j) % getNumSongs());
            }
        }
        return copy;
    }

    public void shuffle() {
        Random rand = new Random();
        {
            for (int i = 0; i < getNumSongs() - 1; i++) {
                int randIndex = rand.nextInt(getNumSongs() - 1);
                Song temp = getSong(i);
                songList.set(i, getSong(randIndex));
                songList.set(randIndex, temp);
            }
        }
    }

    @Override
    public String toString() {
        String tacks = "------------------\n";
        String index;
        int i = 0;
        StringBuilder builder = new StringBuilder();
        builder.append(tacks).append(getName()).
                append(" (").append(getNumSongs()).append(" songs)\n").append(tacks);

        if (getNumSongs() > 0) {
            for (Song s : songList) {
                index = "(" + i + ")";
                builder.append(index).append(" ").append(s.toString()).append("\n");
                i++;
            }
        } else {
            builder.append("There are no songs.\n");
        }
        builder.append(tacks);
        return builder.toString();
    }

    private void loadPlayList(File filename) {
        try {
            Scanner scan = new Scanner(filename);
            name = scan.nextLine().trim();
            while (scan.hasNextLine()) {
                String title = scan.nextLine().trim();
                String artist = scan.nextLine().trim();
                String playtime = scan.nextLine().trim();
                String songPath = scan.nextLine().trim();

                int colon = playtime.indexOf(':');
                int minutes = Integer.parseInt(playtime.substring(0, colon));
                int seconds = Integer.parseInt(playtime.substring(colon + 1));
                int playtimesecs = (minutes * 60) + seconds;

                Song song = new Song(title, artist, playtimesecs, songPath);
                addSong(song);
            }
            scan.close();
        } catch (FileNotFoundException e) {
            System.err.println("Failed to load playlist. " + e.getMessage());
        }
    }

    public String formatPlayTime(int playTime){
        StringBuilder builder = new StringBuilder(4);
        DecimalFormat format = new DecimalFormat("##:00");
        int toMinutes = (playTime % 3600) / 60;
        int toSeconds = (playTime % 60);
        builder.append(format.format(toMinutes));
        builder.append(format.format(toSeconds));

        return builder.toString().substring(0, 5);
    }
}