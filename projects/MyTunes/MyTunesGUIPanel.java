import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by kanna on 10/23/2016.
 */
public class MyTunesGUIPanel extends JPanel {
    public static final int MILLI_PER_SECOND = 1000;
    //    private static final int NUM_COL = 4;
    private int timeRemaining = 0; //use to keep track of the current playtime
    private static final double MAX_PLAYS = 40;
    private final ImageIcon PLAY_BUTTON_ICON = new ImageIcon("images" + File.separator + "play-32.gif");
    private final ImageIcon PLAY_ALL_ICON = new ImageIcon("images" + File.separator + "play-once-32.gif");
    private final ImageIcon PREVIOUS_SONG_ICON = new ImageIcon("images" + File.separator + "media-skip-backward-32.gif");
    private final ImageIcon STOP_BUTTON_ICON = new ImageIcon("images" + File.separator + "stop-32.gif");
    private final ImageIcon SHUFFLE_BUTTON_ICON = new ImageIcon("images" + File.separator + "shuffle-32.gif");
    private final ImageIcon NEXT_SONG_BUTTON_ICON = new ImageIcon("images" + File.separator + "media-skip-forward-32.gif");
    private boolean playing = false;
    private JPanel gridPanel, displayPanel;
    private JLabel nowPlayingLabel, playListInfo;
    private JList<Song> songList;
    private JTable songTable;
    private JButton addSongButton, removeSongButton, playButton, nextSongButton, previousSongButton, shuffleButton, playAll;
    private JButton moveUpButton, moveDownButton;
    private PlayList playList;
    private JButton[][] songButtons;
    private Song[][] songSquare;
    private Timer timer;

    public MyTunesGUIPanel() {
        setLayout(new BorderLayout());
        this.playList = new PlayList(new File("playlist.txt"));
        startAnimation();

        songList = new JList<>(playList.getSongArray());
        songList.setSelectedIndex(0);
        songSquare = playList.getMusicSquare();
        playListInfo = new JLabel(Integer.toString(playList.getNumSongs()) + " songs, " + playList.getPlayTimeString() + " minutes");
        initInfoPanel();
        initDisplayWindow();
        initPlaying();
        initGrid();
    }

    private void initGrid() {
        if (gridPanel != null)
            this.remove(gridPanel);
        songButtons = new JButton[songSquare.length][songSquare.length];
        gridPanel = new JPanel(new GridLayout(songButtons.length, songButtons.length));

        for (int i = 0; i < songButtons.length; i++) {
            for (int j = 0; j < songButtons[i].length; j++) {
                songButtons[i][j] = new JButton(songSquare[i][j].getTitle());
                songButtons[i][j].addActionListener(new SongButtonListener());
                songButtons[i][j].setHorizontalAlignment(SwingConstants.CENTER);
                songButtons[i][j].setFocusPainted(false);
                songButtons[i][j].setBackground(getHeatMapColor(songSquare[i][j].getPlayCount()));
                songButtons[i][j].setToolTipText("Play this song");
                gridPanel.add(songButtons[i][j]);
            }
        }
        add(gridPanel, BorderLayout.CENTER);
        this.revalidate();
    }

    private void initInfoPanel() {
        JPanel mainInfoPanel = new JPanel();
        mainInfoPanel.setLayout(new BoxLayout(mainInfoPanel, BoxLayout.Y_AXIS));
        JPanel namePanel = new JPanel();
        JPanel statsPanel = new JPanel();

        JLabel playListName = new JLabel(playList.getName());
        playListName.setFont(new Font(Font.MONOSPACED, Font.BOLD, 25));

        namePanel.add(playListName);
        statsPanel.add(playListInfo);
        mainInfoPanel.add(namePanel);
        mainInfoPanel.add(statsPanel);
        add(mainInfoPanel, BorderLayout.NORTH);
    }

    private void initDisplayWindow() {
        displayPanel = new JPanel();
        displayPanel.setLayout(new BoxLayout(displayPanel, BoxLayout.Y_AXIS));


        JPanel addRemoveSongPanel = new JPanel();
        String[] columnNames = {"Title", "Artist", "File", "Playtime"};
        songTable = new JTable(playList.getMusicSquare(), columnNames);
        JScrollPane jScrollPane = new JScrollPane(songList);
        songTable.setFillsViewportHeight(true);
        songList.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 15));


        addSongButton = new JButton("Add song");
        addSongButton.setToolTipText("Add a new song to the playlist");
        removeSongButton = new JButton("Remove song");
        removeSongButton.setToolTipText("Remove the selected song from the playlist");

        moveUpButton = new JButton(new ImageIcon("images" + File.separator + "move-up-16.gif"));
        moveUpButton.setToolTipText("Move the selected song up");
        moveDownButton = new JButton(new ImageIcon("images" + File.separator + "move-down-16.gif"));
        moveDownButton.setToolTipText("Move the selected song down");

        moveUpButton.addActionListener(new ButtonControlListener());
        moveDownButton.addActionListener(new ButtonControlListener());
        removeSongButton.addActionListener(new ButtonControlListener());
        addSongButton.addActionListener(new ButtonControlListener());
        addRemoveSongPanel.add(moveUpButton);
        addRemoveSongPanel.add(addSongButton);
        addRemoveSongPanel.add(removeSongButton);
        addRemoveSongPanel.add(moveDownButton);

        displayPanel.add(jScrollPane);
        displayPanel.add(addRemoveSongPanel);

        add(displayPanel, BorderLayout.WEST);
    }

    private void initPlaying() {
        JPanel nowPlayingPanel = new JPanel();
        nowPlayingPanel.setLayout(new BoxLayout(nowPlayingPanel, BoxLayout.Y_AXIS));
        nowPlayingPanel.setBorder(BorderFactory.createTitledBorder("Now Playing"));

        JPanel controlPanel = new JPanel();
        playButton = new JButton();
        playButton.setToolTipText("Play the selected song");
        nextSongButton = new JButton();
        shuffleButton = new JButton();
        playAll = new JButton(PLAY_ALL_ICON);
        playAll.setToolTipText("Play the entire playlist");
        shuffleButton.setToolTipText("Shuffle the entire playlist");
        shuffleButton.setIcon(SHUFFLE_BUTTON_ICON);
        nextSongButton.setToolTipText("Play the next song");
        nextSongButton.setIcon(NEXT_SONG_BUTTON_ICON);
        playButton.setIcon(PLAY_BUTTON_ICON);
        previousSongButton = new JButton();
        previousSongButton.setToolTipText("Play the previous song");
        previousSongButton.setIcon(PREVIOUS_SONG_ICON);

        playButton.addActionListener(new ButtonControlListener());
        previousSongButton.addActionListener(new ButtonControlListener());
        nextSongButton.addActionListener(new ButtonControlListener());
        shuffleButton.addActionListener(new ButtonControlListener());
        playAll.addActionListener(new ButtonControlListener());

        controlPanel.add(playAll);
        controlPanel.add(previousSongButton);
        controlPanel.add(playButton);
        controlPanel.add(nextSongButton);
        controlPanel.add(shuffleButton);

        /* now Playing */
        JPanel playingPanel = new JPanel();
        nowPlayingLabel = new JLabel("Playing:  nothing at the moment");
        playingPanel.add(nowPlayingLabel);

        nowPlayingPanel.add(playingPanel);
        nowPlayingPanel.add(controlPanel);
        add(nowPlayingPanel, BorderLayout.SOUTH);
    }

    private class SongButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < songButtons.length; i++) {
                for (int j = 0; j < songButtons[i].length; j++) {
                    if (e.getSource().equals(songButtons[i][j])) {
                        songList.setSelectedValue(songSquare[i][j], true);
                        playAction(songList.getSelectedIndex());
                    }
                }
            }
        }
    }

//    private class SongListListener implements ListSelectionListener {
//
//        @Override
//        public void valueChanged(ListSelectionEvent e) {
////            songList.getSelectedValue();
//            //TODO: FIX ME LATER
////            Song playing = songList.getSelectedValue();
////            nowPlayingLabel.setText(playing.getTitle() + " by " + playing.getArtist());
//        }
//    }

    private class ButtonControlListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int index = songList.getSelectedIndex();
            JButton source = (JButton) e.getSource();
            if (source.equals(playButton)) {
                if (playing) {
                    stopAction();
                } else {
                    playAction(index);
                }
            } else if (source.equals(nextSongButton)) {
                index++;
                if (index == playList.getNumSongs())
                    index = 0;
                if (playing) {
                    stopAction();
                }
                playAction(index);
            } else if (source.equals(previousSongButton)) {
                index--;
                if (index == -1)
                    index = playList.getNumSongs() - 1;
                if (playing) {
                    stopAction();
                }
                playAction(index);
            } else if (source.equals(moveUpButton)) {
                index = playList.moveUp(songList.getSelectedIndex());
                syncList();
            } else if (source.equals(moveDownButton)) {
                index = playList.moveDown(index);
                syncList();
            } else if (source.equals(removeSongButton)) {
                int answer = JOptionPane.showConfirmDialog(displayPanel, "Are you sure?", "Remove song", JOptionPane.YES_NO_OPTION);
                if (answer == JOptionPane.YES_OPTION) {
                    playList.removeSong(songList.getSelectedIndex());
                    syncList();
                    playListInfo.setText(Integer.toString(playList.getNumSongs()) + " songs, " + playList.getPlayTimeString() + " minutes");
                    initGrid();
                }
            } else if (source.equals(addSongButton)) {
                addNewSong();
                syncList();
                initGrid();
                playListInfo.setText(Integer.toString(playList.getNumSongs()) + " songs, " + playList.getPlayTimeString() + " minutes");
            } else if (source.equals(shuffleButton)) {
                playList.shuffle();
                syncList();
                initGrid();
            } else if (source.equals(playAll)) {
                System.out.println("play all");
            }
            songList.setSelectedIndex(index);
        }
    }

    private class TimerListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            stopAction();
        }
    }

    private class CountDownTimerListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (playing) {
                nowPlayingLabel.setText(playList.getPlaying().getTitle() + " by " +
                        playList.getPlaying().getArtist() + " (" + playList.formatPlayTime(timeRemaining--) + ")");
            }
        }
    }

    private void syncList() {
        songList.setListData(playList.getSongArray());
        songSquare = playList.getMusicSquare();
    }

    private void playAction(int index) {
        songList.setSelectedIndex(index);
        timer = new Timer(songList.getSelectedValue().getPlayTime() * MILLI_PER_SECOND, new TimerListener());
        timer.start();
        playList.playSong(songList.getSelectedValue());
        Song play = playList.getPlaying();
        timeRemaining = play.getPlayTime();
        playButton.setIcon(STOP_BUTTON_ICON);
        playing = true;
        nowPlayingLabel.setText(play.getTitle() + " by " + play.getArtist());

        for (int i = 0; i < songSquare.length; i++) {
            for (int j = 0; j < songSquare[i].length; j++) {
                if (playList.getPlaying().equals(songSquare[i][j])) {
                    songButtons[i][j].setBackground(getHeatMapColor(playList.getPlaying().getPlayCount()));
                }
            }
        }
    }

    private void stopAction() {
        playList.getPlaying().stop();
        timer.stop();
        playButton.setIcon(PLAY_BUTTON_ICON);
        playing = false;
        nowPlayingLabel.setText("Playing: nothing at the moment");
    }

    private void startAnimation() {
        CountDownTimerListener taskPerformer = new CountDownTimerListener();
        new Timer(MILLI_PER_SECOND, taskPerformer).start();
    }

    private void addNewSong() {
        JPanel formInputPanel = new JPanel();
        formInputPanel.setLayout(new BoxLayout(formInputPanel, BoxLayout.Y_AXIS));

        JTextField titleField = new JTextField(20);
        titleField.setToolTipText("Enter the title of the song");
        JTextField artistField = new JTextField(20);
        artistField.setToolTipText("Enter the artist of the song");
        JTextField playTimeField = new JTextField(5);
        playTimeField.setToolTipText("Enter the playtime of the song as MM:SS");

        formInputPanel.add(new JLabel("Title: "));
        formInputPanel.add(titleField);

        formInputPanel.add(new JLabel("Artist: "));
        formInputPanel.add(artistField);

        formInputPanel.add(new JLabel("Playtime: "));
        formInputPanel.add(playTimeField);

        int result = JOptionPane.showConfirmDialog(null, formInputPanel, "Add song",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);


        if (result == JOptionPane.OK_OPTION) {
            String title = titleField.getText();
            String artist = artistField.getText();
            String playTime = playTimeField.getText();
            String fileName = "";
            int playtimesecs = 0;
            try {
                int colon = playTime.indexOf(':');
                int minutes = Integer.parseInt(playTime.substring(0, colon));
                int seconds = Integer.parseInt(playTime.substring(colon + 1));
                playtimesecs = (minutes * 60) + seconds;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Enter playtime in the form of MM:SS");
            }
            JFileChooser jFileChooser = new JFileChooser();

            int status = jFileChooser.showDialog(this, "Add file music file");
            if (status == JFileChooser.APPROVE_OPTION) {
                fileName = jFileChooser.getSelectedFile().getAbsolutePath();
            }
            playList.addSong(new Song(title, artist, playtimesecs, fileName));
        }
    }

    private void playAll() {

    }

    /**
     * Given the number of times a song has been played, this method will
     * return a corresponding heat map color.
     * <p>
     * Sample Usage: Color color = getHeatMapColor(song.getTimesPlayed());
     * <p>
     * This algorithm was borrowed from:
     * http://www.andrewnoske.com/wiki/Code_-_heatmaps_and_color_gradients
     *
     * @param plays The number of times the song that you want the color for has been played.
     * @return The color to be used for your heat map.
     */
    private Color getHeatMapColor(int plays) {
        double minPlays = 0;     // upper/lower bounds
        double value = (plays - minPlays) / (MAX_PLAYS - minPlays); // normalize play count

        // The range of colors our heat map will pass through. This can be modified if you
        // want a different color scheme.
        Color[] colors = {Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED};
        int index1, index2; // Our color will lie between these two colors.
        float dist = 0;     // Distance between "index1" and "index2" where our value is.

        if (value <= 0) {
            index1 = index2 = 0;
        } else if (value >= 1) {
            index1 = index2 = colors.length - 1;
        } else {
            value = value * (colors.length - 1);
            index1 = (int) Math.floor(value); // Our desired color will be after this index.
            index2 = index1 + 1;              // ... and before this index (inclusive).
            dist = (float) value - index1; // Distance between the two indexes (0-1).
        }

        int r = (int) ((colors[index2].getRed() - colors[index1].getRed()) * dist)
                + colors[index1].getRed();
        int g = (int) ((colors[index2].getGreen() - colors[index1].getGreen()) * dist)
                + colors[index1].getGreen();
        int b = (int) ((colors[index2].getBlue() - colors[index1].getBlue()) * dist)
                + colors[index1].getBlue();

        return new Color(r, g, b);
    }
}