import javax.swing.*;
import java.awt.*;

/**
 * Created by kanna on 10/23/2016.
 */
public class MyTunesGUI {
    public static void main(String[] args) {
        JFrame frame = new JFrame("My Tunes");
        frame.setPreferredSize(new Dimension(1500,1000));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.add(new MyTunesGUIPanel());

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}