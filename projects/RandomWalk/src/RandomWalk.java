import java.util.Random;
import java.util.ArrayList;
import java.awt.Point;

/**
 * This represents a random walk within a specified grid size. A seed can be use
 * for debugging purposes.
 * @author Prince Kannah
 */

public class RandomWalk
{
    private int gridSize;
    private Random rand;
    private boolean done;

    public final Point START = new Point(0,0);
    private ArrayList<Point> path;

    /**
     * Constructor: Creates a random walk with the specified grid size; takes no
     * seed.
     * @param gridSize size of grid
     */
    public RandomWalk(int gridSize)
    {
        this.gridSize = gridSize;
        rand = new Random();
        done = false;

        path = new ArrayList<Point>();
        path.add(START);
    }

    /**
     * Constructor: Create a random walk with the specified grid size and seed.
     * @param gridSize size of grid.
     * @param seed seed value
     */
    public RandomWalk(int gridSize, long seed)
    {
        this.gridSize = gridSize;
        rand = new Random(seed);
        done = false;

        path = new ArrayList<Point>();
        path.add(START);
    }

    /**
     * Takes a single step on the grid
     */
    public void step()
    {
        Point current = new Point(path.get(path.size() - 1));

        if(rand.nextBoolean() && current.x != gridSize - 1)
        {
            path.add(new Point(current.x + 1,current.y));
        }
        else if(!rand.nextBoolean() && current.y != gridSize - 1)
        {
            path.add(new Point(current.x,current.y + 1));
        }

        if(current.x == gridSize - 1 && current.y == gridSize - 1)
        {
            done = true;
        }
    }

    /**
     * Creates the walk in one call by using the step() method
     */
    public void createWalk()
    {
        while (!isDone())
        {
            step();
        }
    }

    /**
     * @return Returns the current value of the done variable. Does not check to
     *         see if walk is actually complete.
     */
    public boolean isDone()
    {
        return done;
    }

    /**
     * @return The path of the walk.
     */
    public ArrayList<Point> getPath()
    {
        return path;
    }


    /**
     * @return Return string representation of the walk.
     */
    public String toString()
    {
        String st = "";
        for(Point p : path)
        {
            st += "[" + p.x + "," + p.y + "]";
        }
        return st;
    }
}
