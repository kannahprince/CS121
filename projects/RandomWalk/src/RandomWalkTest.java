import java.util.Scanner;

/**
 * Creates a random walk path that starts at (0,0) and ends at (n-1,n-1) on a
 * specified 2D grid.
 * @author Prince Kannah
 */

public class RandomWalkTest
{
    private static int gridSize = 0;
    private static long seed = 0;
    private static RandomWalk walker;

    /**
     * Get grid size and random seed from user.
     */
    private static void getSeedGrid()
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter grid size: ");
        gridSize = scan.nextInt();

        while (gridSize <= 0)
        {
            System.out.println("Error: gride size must be >= 0!");
            System.out.print("Enter grid size: ");
            gridSize = scan.nextInt();
        };

        System.out.print("Enter a random seed (0 for no seed): ");
        seed = scan.nextLong();

        while (seed < 0)
        {
            System.out.println("random seed must be >= 0!");
            System.out.print("Enter a random seed (0 for no seed): ");
            seed = scan.nextLong();
        }
        scan.close();
    }

    /**
     * main method
     * @param args (not use)
     */
    public static void main(String[] args)
    {
        //call getSeedGrid to process user input
        getSeedGrid();

        // create RandomWalk object using the appropriate constructor
        if(seed == 0)
        {
            walker = new RandomWalk(gridSize);
        }
        else
        {
            walker = new RandomWalk(gridSize,seed);
        }

        // create the random walk and then print it
        walker.createWalk();
        System.out.println(walker);
    }
}
