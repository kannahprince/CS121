# Project 3: RandomWalk in a Concrete Jungle
* Name: Prince Kannah
* Class: CS121
* Date: 3/18/15

## Overview:
RandomWalkTest uses a RandomWalk object to navigates a specified grid size starting from the top left (0,0) and stopping when it reaches the end of the grid at (n-1,n-1). The full path is printed to standard out when the walk is completed.

## Included Files:
All project files are listed in the directory tree below:
    
    ├──README.md
    └──src
        ├──GridMap.java
        ├──RandomWalk.java
        ├──RandomWalkGUI.java
        └──RandomWalkTest.java

## Building & Running:
**_NOTE_: All program files should be in the same directory.**

* To compile (using terminal):
	* `javac RandomWalkTest.java`
	
* To the compiled program (using terminal):
	* `java RandomWalkTest`
	
* To use the RandomWalkGUI, first compile then run:
	* `javac RandomWalkGUI.java`
	
	* `java RandomWalkGUI <grid size> <seed>`

## Program Design:
RandomWalk is the class moving everything behind the scenes. It has two
constructors: one that takes just the grid size and one that accepts the grid
size and a random seed. The latter constructor is helpful in debugging. When an
object is created it is given a starting point (0,0) and that point is then
added to an ArrayList.

The fun beings in our step() method. In
step(), we figure out which direction to go then go that way if we are not out
of bound. Since direction is being determined randomly, separate if statements
are needed to check whether to go right or down. Each step is then added to our
path. If the current x and y are equal to grid size-1 then the walk is complete.
This process is repeated in the creakWalk() method until it reaches (n-1,n-1).
The createWalk() is essential because it saves time since each step doesn’t need
to be coded.

RandomWalkTest is the driver class. It gets a positive grid size and random seed
from a user then creates the proper walk object. The walk is then created using
createWalk() and finally printed out.

## Program Development & Testing:
My very first thought when I started to was create an enum which stores the 2
directions we could move. My thought here was since the number of direction
never changes I can call on my enum to get a direction then move that way. While
I didn’t use this setup it would have brought up some interesting challenges.
For example, how would I randomly pick an enum value.

One challenge I did have was figuring out the logic of step(). While it’s pretty
easy to describe what it should do it prove a bit tricky programming it. That
always seems to be the case. I initially approach it using the starting position
of (0,0) and increase x or y depending on direction. This lead to some weird
coordinates and find myself going back and forth between two coordinates at one
point. This seems to be a common mistake after talking with other students in
the lab. The problem with this approach is that new points are never created
since the x and y are just being incremented. Once we’ve taken a step and added
it to our path the points are no longer useful so I needed to get the current
position of the walk in order to determine how to proceed.

At this point my light bulb came on! I can check my current x and y against grid
size, taking a step in the right direction if they’re not equal. To do this I
had to research how to get the last item in an ArrayList. I first started
testing using the command line and everything worked nicely. Since I could see
the path of the walk with the GUI I used big grid sizes; first 20, then 50 then
100. A moment of pure satisfaction came over me as I watch the blue zig-zag
streak across the grid.

I liked P3. I didn’t find it as challenging at P1 and P2 mostly because I’m now
more familiar with Java (not a master by any means!) and programming concepts
and good practices are in the back of my mind as I code. For example, writing
step() to handle any grid size makes the program dynamic.
