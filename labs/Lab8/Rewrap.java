import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Rewrap
{

    private static Scanner scan;
    private static Scanner fileScanner;
    private static Scanner lineScanner;

    public static void main(String[] args)
    {
        scan = new Scanner(System.in);

        System.out.print("Please enter a plain text file name: ");
        File file = new File(scan.nextLine().trim());

        System.out.print(
                "Please enter the maximum number of characters per line: ");
        final int LINE_MAX = scan.nextInt();

        scan.close();//close scanner since it's no longer needed

        String tmpLine = "";
        int longestLine = Integer.MIN_VALUE;
        int shortestLine = LINE_MAX;

        try
        {
            //scan the file
            fileScanner = new Scanner(file);
            System.out.println("\n" + file
                    + " reformatted with the maximum line length of: "
                    + LINE_MAX + "\n");

            while (fileScanner.hasNextLine())
            {
                //scan each line in the file
                String line = fileScanner.nextLine();
                lineScanner = new Scanner(line);

                while (lineScanner.hasNext())
                {
                    //get each word
                    String token = lineScanner.next();

                    if(tmpLine.length() + token.length() <= LINE_MAX)
                    {
                        tmpLine += token;
                        if(tmpLine.length() + 1 <= LINE_MAX)
                        {
                            tmpLine += " ";
                        }
                    }
                    else
                    {
                        System.out.println(tmpLine);
                        if(tmpLine.length() > longestLine)
                        {
                            longestLine = tmpLine.length();
                        }
                        else if(tmpLine.length() < shortestLine)
                        {
                            shortestLine = tmpLine.length();
                        }
                        tmpLine = token + " ";
                    }
                }
            }
            //to get the last line
            fileScanner.close();
            System.out.println(tmpLine);

            if(tmpLine.length() < shortestLine)
            {
                shortestLine = tmpLine.length();
            }

            System.out.println();
            System.out.println("Longest line: " + longestLine);
            System.out.println("Shortest line: " + shortestLine);

        }
        catch(FileNotFoundException e)
        {
            System.out.println(file + "could not be found.");
            //			System.exit(1);
        }
    }
}
