import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class ParseForCaps
{

    private static final int ERROR_CODE = 1;

    public static void main(String args[])
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a file name: ");

        File file = new File(scan.nextLine().trim());//create new file
        scan.close();

        try
        {
            Scanner fileScan = new Scanner(file);
            System.out.println("\nContents of \"" + file + "\":\n");

            while (fileScan.hasNextLine())
            {
                //read one line
                String line = fileScan.nextLine();

                //create an additional Scanner to break the current line into individual tokens (words) 
                Scanner lineScan = new Scanner(line);

                //read each token from the line until we run out of tokens
                while (lineScan.hasNext())
                {
                    String currentWord = lineScan.next();

                    if(Character.isUpperCase(currentWord.charAt(0)))
                    {
                        System.out.print(currentWord.charAt(0) + " ");
                    }
                }
                lineScan.close();
            }
            fileScan.close();
        }
        catch(FileNotFoundException e)
        {
            System.out.println("File \"" + file + "\" could not be opened.");
            System.exit(ERROR_CODE);
        }
    }
}
