import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class ParseAndRewrap
{

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        //get file name & create new file
        System.out.print("Please enter a plain text file name: ");
        File file = new File(scan.nextLine().trim());

        //get max characters per line
        System.out.print(
                "Please enter the maximum number of characters in a single line: ");
        int maxCharPerLine = scan.nextInt();

        String outputString = "";
        int longestLine = 0;
        int shortestLine = maxCharPerLine;
        scan.close();

        try
        {
            Scanner fileScan = new Scanner(file);
            System.out.println("\n" + file
                    + " reformatted with the maximum line length of: "
                    + maxCharPerLine + "\n");

            while (fileScan.hasNextLine())
            {
                String line = fileScan.nextLine();

                //scan each word
                Scanner lineScan = new Scanner(line);

                while (lineScan.hasNext())
                {
                    String token = lineScan.next();

                    /*
                     * Create the new output String by concatenating tokens
                     * until adding the next token would result in a line longer
                     * than the max length
                     */
                    if(outputString.length() + token.length() <= maxCharPerLine)
                    {
                        outputString += token;//concatenate token to output string

                        /*
                         * Since whitespace is stripped out of tokens by
                         * default, you will need to re-add a space after each
                         * word as you build output Strings if you aren't
                         * already at the maximum length of the line
                         */
                        if(outputString.length() + 1 <= maxCharPerLine)
                        {
                            outputString += " ";
                        }
                    }
                    else
                    {
                        //print the output string
                        System.out.println(outputString);

                        //Keep track of the longest and shortest line in your reformatted output
                        if(outputString.length() >= longestLine)
                        {
                            longestLine = outputString.length();
                        }
                        else
                        {
                            shortestLine = outputString.length();
                        }

                        outputString = token;
                        if(outputString.length() + 1 <= maxCharPerLine)
                        {
                            outputString += " ";
                        }
                    }
                }
                lineScan.close();
            }
            fileScan.close();

            //print the last line
            System.out.println(outputString);

            if(outputString.length() < shortestLine)
            {
                shortestLine = outputString.length();
            }
            System.out.println("\nLongest line: " + longestLine);
            System.out.println("Shortest line: " + shortestLine);
        }
        catch(FileNotFoundException e)
        {
            System.out.println(e.getMessage());
        }

    }

}
