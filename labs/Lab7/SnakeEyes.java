import java.util.Scanner;

/**
 * Demonstrates the use of a programmer-defined class.
 * @author Java Foundations
 */
public class SnakeEyes
{
    /**
     * Creates two Die objects and rolls them several times, counting the number
     * of snake eyes that occur.
     * @param args (unused)
     */
    public static void main(String[] args)
    {
        final int ROLLS = 5000;
        int fValueOne, fValueTwo, snakeEyeCount = 0;
        Die die1, die2;

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter number of sides: ");
        int numSides = sc.nextInt();

        System.out.print("Enter a seed (or 0 for no seed): ");
        long seed = sc.nextLong();

        if(seed == 0)
        {
            die1 = new Die(numSides);
            die2 = new Die(numSides);
        }
        else
        {
            die1 = new Die(numSides,seed);
            die2 = new Die(numSides,seed);
        }

        for(int roll = 1; roll <= ROLLS; roll++)
        {
            fValueOne = die1.roll();
            fValueTwo = die2.roll();

            //print the value of die1 and die2
            System.out.println("roll " + roll);

            System.out.println("die1 value: " + die1);
            System.out.println("die2 value: " + die2);

            System.out.println();

            if(fValueOne == 1 && fValueTwo == 1) // check for snake eyes
                snakeEyeCount++;
        }

        System.out.println("Number of rolls: " + ROLLS);
        System.out.println("Number of snake eyes: " + snakeEyeCount);
        System.out.println("Ratio: " + (float) snakeEyeCount / ROLLS);
        sc.close();//at last
    }
}
