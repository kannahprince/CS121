import java.util.Scanner;

public class DiceRoller
{

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        PairOfDice dice = new PairOfDice(6);

        String rollAgain = null;
        //		boolean again = false;

        int userWin = 0;
        int computerWin = 0;
        int tie = 0;

        // for extra interest
        int totalUserWin = 0;
        int totalComputerWin = 0;
        int totalTie = 0;

        do
        {
            int userRoll = dice.roll();
            System.out.println(
                    "Your roll: " + userRoll + " " + "(" + dice.getFaceValue1()
                            + " + " + dice.getFaceValue2() + ")");
            int computerRoll = dice.roll();
            System.out.println("Computer roll: " + computerRoll + " " + "("
                    + dice.getFaceValue1() + " + " + dice.getFaceValue2()
                    + ")");

            if(userRoll == computerRoll)
            {
                System.out.println("It's a tie!");
                tie++;
                totalTie += tie;
            }
            else if(userRoll > computerRoll)
            {
                System.out.println("You win!");
                userWin++;
                totalUserWin += userWin;
            }
            else
            {
                System.out.println("You lose!");
                computerWin++;
                totalComputerWin += computerWin;
            }
            System.out.println();// space

            System.out.println(
                    "Your wins: " + userWin + "   " + "Computer's wins: "
                            + computerWin + "   " + "Ties: " + tie + "\n");

            System.out.println(
                    "Do you want to roll again? (Y)es to continue, anything else to quit.");
            rollAgain = sc.nextLine();
            System.out.println();// space

            //			if(rollAgain.equalsIgnoreCase("y")){
            //				again = true;
            //			}

        } while (rollAgain.equalsIgnoreCase("y"));

        System.out.println();
        if(totalComputerWin > totalUserWin)
        {
            System.out.print(
                    "Thanks for playing! better luck next time...loser \n");
        }
        else
        {
            System.out.print("Nice game. Thanks for playing! \n");
        }

        System.out
                .println("Total wins: " + totalUserWin + "   " + "Total lost: "
                        + totalComputerWin + "   " + "Total tie: " + totalTie);

        sc.close();// at last
    }

}
