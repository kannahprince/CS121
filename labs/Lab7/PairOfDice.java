/**
 * The <code>PairOfDice</code> class represents a dice. Each dice has a number
 * of sides defined by a user. A seed can also be provided for debugging. Dice
 * can be created as follow:
 * <p>
 * <blockquote>
 * 
 * <pre>
 *     PairOfDice dice = new PairOfDice(6). //without a seed
 *     PairOfDice dice = new PairOfDice(6,10,10). //with a seed
 * </pre>
 * 
 * </blockquote>
 * <p>
 * An example of a PairOfDice being use:
 * <p>
 * <blockquote>
 * 
 * <pre>
 * System.out.println(dice.roll());
 * System.out.println(dice.getFaceValue1());
 * </pre>
 * 
 * </blockquote>
 * <p>
 * @author Prince Kannah
 */

public class PairOfDice
{

    // instance variables
    private Die die1, die2;

    /**
     * Constructor: Create a pair of dice with the specified number of sides.
     * @param sides The number of sides for each die
     */
    public PairOfDice(int sides)
    {
        die1 = new Die(sides);
        die2 = new Die(sides);
    }

    /**
     * Constructor: Create a pair of dice with the specified number of sides and
     * seed.
     * @param sides The number of sides for each die
     * @param seed1 Seed value for first die.
     * @param seed2 Seed value for second die.
     */
    public PairOfDice(int sides, long seed1, long seed2)
    {
        die1 = new Die(sides,seed1);
        die2 = new Die(sides,seed2);
    }

    /**
     * Rolls the dice and return the sum of their face values.
     * @return The sum of the face values for the <code>PairOfDice</code>.
     */
    public int roll()
    {
        return die1.roll() + die2.roll();
    }

    /**
     * @return Returns the current sum of the face values for the
     *         <code>PairOfDice</code>.
     */
    public int getTotal()
    {
        return getFaceValue1() + getFaceValue2();
    }

    /**
     * @return The face value of the first die
     */
    public int getFaceValue1()
    {
        return die1.getFaceValue();
    }

    /**
     * @return The face value of the second die
     */
    public int getFaceValue2()
    {
        return die2.getFaceValue();
    }

    /**
     * @return A string representation of the PairOfDice.
     */
    public String toString()
    {
        return String.format("Sum of face value: " + roll()
                + "Die 1 face value: " + getFaceValue1() + "Die 2 face value:"
                + getFaceValue2());

    }

}
