import java.util.Random;

/**
 * Die.java Java Foundations Represents one die (singular of dice) with faces
 * showing values
 * @author Modified by Prince Kannah
 */

public class Die
{

    private Random random;
    private int numSides;
    private int faceValue;// current value showing on the die

    /**
     * Constructor: create a new die using the specified number sides
     * @param numSides the number of sides
     */
    public Die(int numSides)
    {
        this.numSides = numSides;
        faceValue = 1;
        random = new Random();
    }

    /**
     * Constructor: create a new die using specified number sides and seed value
     * @param numSides the number of sides
     * @param seed the seed value
     */
    public Die(int numSides, long seed)
    {
        this.numSides = numSides;
        faceValue = 1;
        random = new Random(seed);
    }

    /**
     * Computes a new face value for this die and returns the result.
     * @return The new face value.
     */
    public int roll()
    {
        faceValue = random.nextInt(numSides) + 1;
        return faceValue;
        //	   return random.nextInt(numSides) + 1;
    }

    /**
     * Face value mutator. The face value is not modified if the specified value
     * is not valid.
     * @param value The new face value. Must be between 1 and max face value.
     */
    public void setFaceValue(int value)
    {
        if(value > 0 && value <= this.numSides)
        {
            faceValue = value;
        }
    }

    /**
     * Returns the face value of the die
     * @return The current face value.
     */
    public int getFaceValue()
    {
        return faceValue;
    }

    /**
     * Returns the number of sides of the die.
     * @return The current number of sides
     */
    public int getNumSides()
    {
        return numSides;
    }

    /**
     * Returns a string representation of this die.
     */
    public String toString()
    {
        String str = "Die [numSides = " + numSides + ", faceValue = "
                + faceValue + "]";
        return str;
    }
}
