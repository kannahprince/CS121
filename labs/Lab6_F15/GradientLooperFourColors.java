import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GradientLooperFourColors extends JPanel
{

    /*
     * This method draws on the Graphics context. This is where your work will
     * be.
     *
     * (non-Javadoc)
     * 
     * @see java.awt.Container#paint(java.awt.Graphics)
     */
    public void paintComponent(Graphics canvas)
    {
        //ready to paint
        super.paintComponent(canvas);

        //account for changes to window size
        int width = getWidth();// panel width
        int height = getHeight();// panel height

        final int GRADIENT_DIVISIONS = 255;
        final int NUM_GRADIENT_BARS = 4;
        final int barHeight = height / NUM_GRADIENT_BARS;
                
        double rectWidth = (double) width / GRADIENT_DIVISIONS;
        double x = Math.ceil(((double) width / GRADIENT_DIVISIONS));
        int y = 0;

        int red = 0;
        int green = 0;
        int blue = 0;

        //TODO: Your code goes here
        for(int i = 0; i < NUM_GRADIENT_BARS; i++)
        {
            for(int j = 0; j < GRADIENT_DIVISIONS; j++)
            {
                canvas.setColor(new Color(red,green,blue));
                canvas.fillRect(j*(int)x, i *(y +barHeight),(int)rectWidth +1,barHeight);

                switch (i)
                {
                    case 0://grey bar
                        red++;
                        green++;
                        blue++;
                    break;
                    case 1://red bar
                        red++;
                    break;
                    case 2:// gren bar
                        green++;
                    break;
                    case 3:// blue bar
                        blue++;
                }
            }
            red = 0;
            green = 0;
            blue = 0;
        }

    }

    /**
     * DO NOT MODIFY Constructor for the display panel initializes necessary
     * variables. Only called once, when the program first begins.
     */
    public GradientLooperFourColors()
    {
        setBackground(Color.black);
        int initWidth = 768;
        int initHeight = 512;
        setPreferredSize(new Dimension(initWidth,initHeight));
        this.setDoubleBuffered(true);
    }

    /**
     * DO NOT MODIFY Starting point for the program
     * @param args unused
     */
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("GradientLooperFourColors");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new GradientLooperFourColors());
        frame.pack();
        frame.setVisible(true);
    }
}
