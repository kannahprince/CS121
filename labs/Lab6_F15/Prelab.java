
public class Prelab
{
    public static void main(String[] args)
    {
        final int ROW_MAX = 100;

        for(int i = 1; i <= ROW_MAX; i++)
        {
            for(int j = ROW_MAX - 1; j >= i; j--)
            {
                System.out.print(" ");
            }
            for(int k = 1; k <= i; k++)
            {
                System.out.print("*");
            }
            System.out.println();
        }
    }

}
