import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GradientLooperColorsVertical extends JPanel
{
    /*
     * This method draws on the Graphics context. This is where your work will
     * be.
     *
     * (non-Javadoc)
     * 
     * @see java.awt.Container#paint(java.awt.Graphics
     */
    public void paintComponent(Graphics canvas)
    {
        //ready to paint
        super.paintComponent(canvas);

        //account for changes to window size
        int width = getWidth();
        int height = getHeight();

        final int GRADIENT_DIVISIONS = 256;
        final int NUM_GRADIENT_BARS = 7;

        //the width of each vertical bar
        double barWidth = Math.ceil((double) width / NUM_GRADIENT_BARS);

        //double rectWidth = (double) width / GRADIENT_DIVISIONS;
        double y = (double) width / GRADIENT_DIVISIONS;
        int x = 0;
        int r = GRADIENT_DIVISIONS - 1, g = GRADIENT_DIVISIONS - 1,
                b = GRADIENT_DIVISIONS - 1;

        for(int k = 0; k < NUM_GRADIENT_BARS; k++)
        {
            for(int i = 0; i < GRADIENT_DIVISIONS; i++)
            {
                canvas.setColor(new Color(r,g,b));
                canvas.fillRect(k * ((int) barWidth + x),i * (int) y,
                        (int) barWidth,height);

                switch (k) //total number of bar
                {
                    case 0://grey
                        r--;
                        g--;
                        b--;
                    break;
                    case 1://cyan
                        g--;
                        b--;
                        ;
                    break;//blue
                    case 2:
                        b--;
                    break;
                    case 3://magenta
                        r--;
                        b--;
                    break;
                    case 4://red
                        r--;
                    break;
                    case 5://yellow
                        r--;
                        g--;
                    break;
                    case 6://green
                        g--;
                }
            }
            r = GRADIENT_DIVISIONS - 1;
            g = GRADIENT_DIVISIONS - 1;
            b = GRADIENT_DIVISIONS - 1;
        }
    }

    /**
     * DO NOT MODIFY Constructor for the display panel initializes necessary
     * variables. Only called once, when the program first begins.
     */
    public GradientLooperColorsVertical()
    {
        setBackground(Color.black);
        int initWidth = 768;
        int initHeight = 512;
        setPreferredSize(new Dimension(initWidth,initHeight));
        this.setDoubleBuffered(true);
    }

    /**
     * DO NOT MODIFY Starting point for the program
     * @param args unused
     */
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("GradientLooperColorsVertical");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new GradientLooperColorsVertical());
        frame.pack();
        frame.setVisible(true);
    }
}