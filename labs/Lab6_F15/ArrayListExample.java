import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

/**
 * Use an ArrayList to store a collection of objects and use a for-each loop to
 * process the objects.
 * @author Prince Kannah
 */
public class ArrayListExample
{

    public static void main(String[] args)
    {
        Random rand = new Random();
        DecimalFormat dfmt = new DecimalFormat("0.00");
        final int RADIUS_MAX = 100;
        final int NUM_SPHERES = 100;

        //TODO: Declare the ArrayList to hold the Sphere objects
        ArrayList<Sphere> sphereList = new ArrayList<Sphere>();
        
        //TODO: Create the spheres using a normal for loop and add them to an ArrayList<Sphere>
        for(int i = 1; i <= NUM_SPHERES; i++)
        {
            sphereList.add(new Sphere(rand.nextInt(RADIUS_MAX)));
        }
        
        for(Sphere s : sphereList)
        {
            System.out.println("Sphere " + sphereList.indexOf(s) + ":\tradius: "
                    + dfmt.format(s.getRadius()) + "\tsurface area: "
                    + dfmt.format(s.getSurfaceArea()) + "\tvolume: "
                    + dfmt.format(s.getVolume()));
        }

        double max_volume = Double.MAX_VALUE;
        
        for(Sphere s : sphereList)
        {
            if(s.getVolume() <= max_volume)
            {
                max_volume = s.getVolume();
            }
        }
        System.out.printf("\nVolume of the smallest sphere: %.2f\n",max_volume);
    }
}