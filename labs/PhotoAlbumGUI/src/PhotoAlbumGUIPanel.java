import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Displays a JList of Photos and a preview of the photo.
 * This class manages layout of controls and also handles events.
 *
 * @author CS121 Instructors
 * @author You
 */
@SuppressWarnings("serial")
public class PhotoAlbumGUIPanel extends JPanel
{
	/** The data representing the list of photos in the album (the "model") */
	private PhotoAlbum album;
    PhotoListListener selectionListener;

	/** The GUI representation of the list of photos in the album (the "view") */
	private JList<Photo> photoList;

	private JLabel imageLabel;  // Displays the image icon inside of the preview panel
	private JButton nextButton; // Selects the next image in the photoList
	private JButton prevButton; // Selects the previous image in the photoList */

	/**
	 * Instantiates the photo album and adds all of the components to the JPanel.
	 */
	public PhotoAlbumGUIPanel()
	{
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		// Instantiate the album object and print it to the command line
		//      to make sure it worked.
		album = new PhotoAlbum("Boise", "photos/album.dat");
		//System.out.println(album);

		// TODO: Instantiate the JList<Photo> photoList object (declared above) and
		//      set the list data to album.getPhotoArray().
		//      Set the selected index of the photoList to position 0 to select the
		//      first photo by default.
        photoList = new JList<>(album.getPhotoArray());
        photoList.setSelectedIndex(0);

		// TODO: Add a ListSelectionListener to the photoList
        selectionListener = new PhotoListListener();
        photoList.addListSelectionListener(selectionListener);

		// TODO: Create a JScrollPane containing the photoList.
        JScrollPane pane = new JScrollPane(photoList);

		// TODO: Add the scrollPane to this panel.
        add(pane);

		// TODO: Create a new preview sub-panel.
        JPanel previewPanel = new JPanel();
		// TODO: Instantiate the imageLabel (declared above) as an empty JLabel().
        imageLabel = new JLabel();

		// TODO: Add the imageLabel to the previewPanel and add the previewPanel
		//      to this panel.
        previewPanel.add(imageLabel);
        add(previewPanel);

		// TODO: Use the displayPhoto() method (provided below) to display the
		//      photo currently selected in the photoList.
        displayPhoto(photoList.getSelectedValue());

		// TODO: Update the valueChanged method of the PhotoListListener (below) to
		//       display the selected photo whenever you click a new photo in the list.

		// TODO: Create a sub-panel for control buttons.
        JPanel controlPanel = new JPanel();

		// TODO: Initialize the prevButton and nextButton and add the same
		//      ControlListener to both.
        prevButton = new JButton("<");
        prevButton.addActionListener(new ControlListener());
        nextButton = new JButton(">");
        nextButton.addActionListener(new ControlListener());
        controlPanel.add(prevButton);
        controlPanel.add(nextButton);

		// TODO: Add both buttons to the controlPanel and add the controlPanel
		//       to this panel.
        add(controlPanel);
	}

	private class PhotoListListener implements ListSelectionListener
	{
		/* (non-Javadoc)
		 * @see java.awt.event.ListSelectionListener#valueChanged(java.awt.event.ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent event)
		{
			//TODO: Use the displayPhoto() method (provided below) to display the
			// photo currently selected in the photoList.
            displayPhoto(photoList.getSelectedValue());
		}
	}

	private class ControlListener implements ActionListener
	{
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent event)
		{
			// Find index of photo that is currently selected.
			int index = photoList.getSelectedIndex();
            JButton source = (JButton) event.getSource();
            if (source.getText().equalsIgnoreCase("<"))
                index--;
            else
                index++;
			// TODO: Update the index depending on which button was clicked.
			photoList.setSelectedIndex(index);
		}
	}

	/**
	 * Updates the photo on the preview panel.
	 * @param photo The photo to display.
	 */
	private void displayPhoto(Photo photo)
	{
		try {
			ImageIcon icon = new ImageIcon(ImageIO.read(photo.getFile()));
			imageLabel.setIcon(icon);
		} catch (IOException ex) {
			imageLabel.setText("Image not found :(");
		}
	}
}