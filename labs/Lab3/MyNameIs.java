
import java.util.Scanner;
import java.text.NumberFormat;
import java.text.DecimalFormat;

/**
 * A simple application to test use of String, Math, DecimalFormat and NumberFormat classes.
 * @author 
 */
public class MyNameIs {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		Scanner keyboard = new Scanner(System.in);
		DecimalFormat dfrmt = new DecimalFormat("0.00");
		NumberFormat nfrmt = NumberFormat.getPercentInstance();
		
		System.out.print("First name: ");
		String firstName = keyboard.nextLine();
		
		System.out.print("Last name: ");
		String lastName = keyboard.nextLine();
		
		System.out.print("Enter a number: ");
		double n1 = keyboard.nextDouble();
		
		System.out.print("Enter another number (between 0 and 1): ");
		double n2 = keyboard.nextDouble();
		
		System.out.println("\nHi, my name is " + firstName + " " + lastName + ".");
		System.out.println("\nYou'll find me under: \"" + lastName + ", "+ firstName + "\"");
		System.out.println("\nMy name badge: " + firstName.substring(0,1) + ". " + lastName);
		
		double n1_n2 = n1*n2;
		double raiseToPower = Math.pow(n1, n2);
		System.out.println(nfrmt.format(n2) + " of " + dfrmt.format(n1) + " is " + dfrmt.format(n1_n2));
		System.out.println(dfrmt.format(n1) + " raise to the power of " + dfrmt.format(n2) + " is " + dfrmt.format(raiseToPower));
		
		keyboard.close();
	}
}
