import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * CS 121 Lab: Conditionals and Loops 2
 * @author Prince Kannah
 */
public class GoingLoopy
{

    public static void main(String[] args)
    {
        DecimalFormat fmt = new DecimalFormat("0.00");
        /*
         * Multiples of 7 between 0 and 100 using while loop
         * 
         * Use a while loop to evaluate every integer from 0 to 100 to see which
         * are evenly divisible by 7. Print the discovered multiples of 7 as you
         * go and, afterward, print the total number of multiples of 7.
         */
        System.out.println(
                "multiples of 7 between 0 and 100 (using while loop):");
        int count = 0;

        //TODO: Your code goes here
        int lCounter = 0;
        while (count <= 100)
        {
            if(count % 7 == 0)
            {
                System.out.print(count + " ");
                lCounter++;
            }
            count++;
        }
        System.out.println("\ntotal: " + lCounter);
        System.out.println();

        /*
         * Multiples of 7 between 0 and 100 using while loop
         * 
         * Use a for loop to accomplish exactly the same task as in the previous
         * while loop.
         */
        System.out
                .println("multiples of 7 between 0 and 100 (using for loop):");
                //		int count2 = 0;

        //TODO: Your code goes here
        int fCounter = 0;
        for(int count2 = 0; count2 <= 100; count2++)
        {
            if(count2 % 7 == 0)
            {
                System.out.print(count2 + " ");
                fCounter++;
            }
        }
        System.out.println("total: " + fCounter);
        System.out.println();

        /*
         * Multiples of 7 between 0 and 100 using do loop
         * 
         * Use a do loop to accomplish exactly the same task as in the previous
         * while and for loops.
         */
        System.out.println("multiples of 7 between 0 and 100 (using do loop):");
        int count3 = 0;

        //TODO: Your code goes here
        int dCounter = 0;
        do
        {
            if(count3 % 7 == 0)
            {
                System.out.print(count3 + " ");
                dCounter++;
            }
            count3++;
        } while (count3 <= 100);
        System.out.println("total: " + dCounter);
        System.out.println();

        /*
         * Populate an ArrayList in a loop
         * 
         * In a loop (your choice) create 5 Sphere objects, with diameters 1, 2,
         * ... 10. Add them to ArrayList<Sphere> spheres, using its add()
         * method. spheres.add( s ) adds Sphere s to the collection
         */
        ArrayList<Sphere> spheres = new ArrayList<Sphere>();

        //TODO: Your code goes here
        for(int i = 1; i <= 5; i++)
        {
            spheres.add(new Sphere(i * 2));
        }
        /*
         * Print contents of an ArrayList with a for-each loop
         * 
         * Use a for-each loop to print the radius, surface area, and volume of
         * each Sphere s from ArrayList<Sphere> spheres.
         */
        System.out.println("radius, surface area, and volume of 5 spheres:");

        //TODO: Your code goes here
        for(Sphere s : spheres)
        {
            System.out.println("radius: " + fmt.format(s.getRadius())
                    + "\nsurface area: " + fmt.format(s.getSurfaceArea())
                    + "\nvolume: " + fmt.format(s.getVolume()) + "\n");
            //            System.out.println();
        }

        /*
         * Build a 2D table of values in a nested for loop
         * 
         * Build a table using a nested for loop. The outer loop will iterate
         * over 5 rows and the inner loop will iterate over 10 columns. Print
         * each value in the table as the row number times the column number.
         * Create variables or constants for the maximum row and column values
         * rather than hard-coding numeric literals in your condition
         * statements. Sample output: 1 2 3 4 5 6 7 8 9 10 2 4 6 8 10 12 14 16
         * 18 20 3 6 9 12 15 18 21 24 27 30 4 8 12 16 20 24 28 32 36 40 5 10 15
         * 20 25 30 35 40 45 50
         */

        //TODO: Your code goes here
        final int ROW = 6;
        final int COL = 11;
        int[][] grid = new int[ROW][COL];

        //nested loop to fill in the array & print
        for(int p = 0; p < grid.length; p++)
        {
            for(int j = 0; j < grid[p].length; j++)
            {
                grid[p][j] = p * j;
            }
        }

        //nest loop to print out the array
        for(int k = 1; k < grid.length; k++)
        {
            for(int l = 1; l < grid[k].length; l++)
            {

                System.err.print(String.format("%-3d",grid[k][l]));
            }
            System.out.println();
        }
    }
}
