import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Draws gradients across the width of the panel
 * @author ?
 */
@SuppressWarnings("serial")
public class GradientLooperV2 extends JPanel
{

    /*
     * This method draws on the Graphics context. This is where your work will
     * be.
     * 
     * (non-Javadoc)
     * 
     * @see java.awt.Container#paint(java.awt.Graphics)
     */
    public void paintComponent(Graphics canvas)
    {
        // ready to paint
        super.paintComponent(canvas);
        canvas.setColor(getBackground());
        // account for changes to window size
        int width = getWidth();// panel width
        int height = getHeight();// panel height

        //x and y for the anchor point of the rectangles
        int x = 0;
        int y = 0;

        //integers for the color of the rectangles that make up the divisions
        int red = 0;
        int green = 0;
        int blue = 0;

        final int GRADIENT_DIVISIONS = 256;
        final int NUM_GRADIENTS = 4;

        //GRAYSCALE ONLY
        // TODO: Your code goes here
        // for(int i = 0; i < GRADIENT_DIVISIONS - 1; i++){
        // canvas.setColor(new Color(red, green, blue));
        // canvas.fillRect(x, y, width/256, height);
        // x += width/256;
        // red++;
        // green++;
        // blue++;
        //
        // }

        // For-loop for all of the rectangles
        int rectW = width / 255;
        int rectH = height / 4;

        //loops through for the number of divisions (4 in our case).
        for(int i = 0; i < NUM_GRADIENTS; i++)
        {
            for(int j = 0; j < GRADIENT_DIVISIONS; j++)
            {//creates all 255 rectangles for one division
                canvas.setColor(new Color(red,green,blue));
                canvas.fillRect(x,y,rectW,rectH);
                x += rectW;

                if(i == 0)
                {
                    red++;
                    blue++;
                    green++;
                }
                else if(i == 1)
                {
                    red++;
                }
                else if(i == 2)
                {
                    green++;
                }
                else if(i == 3)
                {
                    blue++;
                }

            }

            //reset all of the variables for the next division
            red = 0;
            green = 0;
            blue = 0;
            x = 0;
            y += rectH;//move the y position to the appropriate starting point of the next division
        }

        // //grayscale rectangle
        // int grayX = 0;
        // int grayY = 0;
        // int grayR = 0;
        // int grayG = 0;
        // int grayB = 0;
        //
        // for(int j = 0; j < GRADIENT_DIVISIONS; j++){
        // canvas.setColor(new Color(grayR, grayG, grayB));
        // canvas.fillRect(grayX, grayY, width/256, height/4);
        // grayX += width/256;
        // grayR++;
        // grayG++;
        // grayB++;
        // }
        //
        // //red rectangle
        // int redX = 0;
        // int redY = height/4;
        // int redR = 0;
        // int redG = 0;
        // int redB = 0;
        //
        // for(int j = 0; j < GRADIENT_DIVISIONS; j++){
        // canvas.setColor(new Color(redR, redG, redB));
        // canvas.fillRect(redX, redY, width/256, height/4);
        // redX += width/256;
        // redR++;
        // }
        //
        // //green rectangle
        // int greenX = 0;
        // int greenY = height / 2;
        // int greenR = 0;
        // int greenG = 0;
        // int greenB = 0;
        //
        // for(int k = 0; k < GRADIENT_DIVISIONS; k++){
        // canvas.setColor(new Color(greenR, greenG, greenB));
        // canvas.fillRect(greenX, greenY, width/256, height/4);
        // greenX += width/256;
        // greenG++;
        // }
        //
        // //blue rectangle
        // int blueX = 0;
        // int blueY = height * 3 / 4;
        // int blueR = 0;
        // int blueG = 0;
        // int blueB = 0;
        //
        // for(int l = 0; l < GRADIENT_DIVISIONS; l++){
        // canvas.setColor(new Color(blueR, blueG, blueB));
        // canvas.fillRect(blueX, blueY, width/256, height/4);
        // blueX += width/256;
        // blueB++;
        // }
        //

    }

    /**
     * DO NOT MODIFY Constructor for the display panel initializes necessary
     * variables. Only called once, when the program first begins.
     */
    public GradientLooperV2()
    {
        setBackground(Color.black);
        int initWidth = 768;
        int initHeight = 512;
        setPreferredSize(new Dimension(initWidth,initHeight));
        this.setDoubleBuffered(true);
    }

    /**
     * DO NOT MODIFY Starting point for the program
     * @param args unused
     */
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("GradientLooper");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new GradientLooperV2());
        frame.pack();
        frame.setVisible(true);
    }
}