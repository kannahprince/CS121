import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Draws gradients across the width of the panel
 * @author ?
 */
@SuppressWarnings("serial")
public class GradientLooper extends JPanel
{

    /*
     * This method draws on the Graphics context. This is where your work will
     * be.
     *
     * (non-Javadoc)
     * 
     * @see java.awt.Container#paint(java.awt.Graphics)
     */
    public void paintComponent(Graphics canvas)
    {
        //ready to paint
        super.paintComponent(canvas);

        //account for changes to window size
        int width = getWidth();// panel width
        int height = getHeight();// panel height
        int y = 0;

        final int GRADIENT_DIVISIONS = 256;
        final int NUM_GRADIENTS = 7;
        double x = Math.ceil((double) width / GRADIENT_DIVISIONS);
        double rectWidth = Math.ceil((double) width / GRADIENT_DIVISIONS);
        final int mBarHeight = height / NUM_GRADIENTS;

        int red = 1;
        int green = 1;
        int blue = 1;

        //TODO: Your code goes here
        //GREYSCALE
        //        for(int j = 0; j < GRADIENT_DIVISIONS - 1; j++)
        //        {
        //            canvas.setColor(new Color(green++,red++,blue++));
        //            //canvas.setColor(Color.GRAY);
        //            canvas.fillRect(x,y,width + 256,height);
        //            x += width / 256;
        //            red++;
        //            green++;
        //            blue++;
        //        }

        for(int i = 0; i < NUM_GRADIENTS; i++)
        {
            for(int j = 0; j < GRADIENT_DIVISIONS - 1; j++)
            {
                canvas.setColor(new Color(red,green,blue));
                canvas.fillRect((int) x,y,(int) rectWidth,height);
                x += rectWidth;

                switch (i)
                {
                    case 0://grey bar
                        red++;
                        green++;
                        blue++;
                    break;
                    case 1://red bar
                        red++;
                    break;
                    case 2:// yellow bar
                        red++;
                        green++;
                    break;
                    case 3:// green bar
                        green++;
                    break;
                    case 4://cyan bar
                        green++;
                        blue++;
                    break;
                    case 5:
                        blue++;
                    break;
                    case 6://purple/magenta bar
                        red++;
                        blue++;
                    break;
                }
            }
            red = 0;
            green = 0;
            blue = 0;
            y += mBarHeight;
            x = 0;
        }
    }

    /**
     * DO NOT MODIFY Constructor for the display panel initializes necessary
     * variables. Only called once, when the program first begins.
     */
    public GradientLooper()
    {
        setBackground(Color.black);
        int initWidth = 768;
        int initHeight = 512;
        setPreferredSize(new Dimension(initWidth,initHeight));
        this.setDoubleBuffered(true);
    }

    /**
     * DO NOT MODIFY Starting point for the program
     * @param args unused
     */
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("GradientLooper");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new GradientLooper());
        frame.pack();
        frame.setVisible(true);
    }
}
