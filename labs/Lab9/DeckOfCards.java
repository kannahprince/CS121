import java.util.Random;

public class DeckOfCards implements DeckOfCardsInterface {
    private final int DECK_SIZE = 52;
    private Card[] deck;
    private int nextCardIndex;

    /**
     * Constructor: Creates standard deck of playing cards.
     */
    public DeckOfCards() {
        restoreDeck();
    }

    @Override
    public void shuffle() {
        Random generator = new Random();
        for(int i = 0; i < deck.length; i++)
        {
            int j = generator.nextInt(deck.length);
            Card temp = deck[i];
            deck[i] = deck[j];
            deck[j] = temp;
        }
        nextCardIndex = 0;
    }

    /**
     * Draw a single Card from the deck if the deck is not empty.
     * @return the card drawn from the Deck
     */
    @Override
    public Card draw() {
        if(nextCardIndex < DECK_SIZE)
        {
            nextCardIndex++;
            return deck[nextCardIndex - 1];
        }
        return null;
    }

    /**
     * Returns/restore the deck to its "out of box" state
     */
    @Override
    public void restoreDeck() {
        deck = new Card[DECK_SIZE];
        int index = 0;
        for(Suit s : Suit.values())
        {
            for(FaceValue value : FaceValue.values())
            {
                deck[index] = new Card(s,value);
                index++;
            }
        }
        nextCardIndex = 0;
    }

    /**
     * Get the number of remaining cards in the deck
     * @return the number of cards remaining in deck
     */
    @Override
    public int numCardsRemaining() {
        return DECK_SIZE - nextCardIndex;
    }

    /**
     * Get the number of cards that have been dealt from the deck.
     * @return the number of cards that have been dealt.
     */
    @Override
    public int numCardsDealt() {
        return nextCardIndex;
    }

    /**
     * Get the collection of cards that have been dealt from the deck
     * @return an array of the cards that be been dealt.
     */
    @Override
    public Card[] dealtCards() {
        Card[] dealtCards = new Card[numCardsDealt()];
        for(int i = 0; i < dealtCards.length; i++)
        {
            dealtCards[i] = deck[i];
        }
        return dealtCards;
    }

    /**
     * Get the collection of cards that have NOT been dealt.
     * @return an array of the remaining cards in the deck.
     */
    @Override
    public Card[] remainingCards() {
        Card[] remainingCards = new Card[numCardsRemaining()];
        int j = nextCardIndex;
        for(int i = 0; i < remainingCards.length; i++)
        {
            remainingCards[i] = deck[j];
            j++;
        }
        return remainingCards;
    }

    @Override
    public String toString() {
        StringBuilder buildString = new StringBuilder();
        int i = 0;
        for(Card card : remainingCards())
        {
            if(i % 4 == 0)
            {
                buildString.append("\n");
            }
            buildString.append(card + " ");

            i++;
        }
        return buildString.toString();
    }
}