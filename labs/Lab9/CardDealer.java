
public class CardDealer {
    public static void main(String[] args)
    {
        DeckOfCards deck = new DeckOfCards();
        System.out.print("Fresh, un-shuffled deck:");
        System.out.println(deck + "\n");

        //shuffle then print
        System.out.print("Shuffled deck:");
        deck.shuffle();
        System.out.println(deck + "\n");

        while (deck.numCardsRemaining() != 0)
        {
            Card player1, player2;
            player1 = deck.draw();
            player2 = deck.draw();

            System.out.println("Player 1: " + player1);
            System.out.println("Player 2: " + player2);

            if(player1.compareTo(player2) == -1)
            {
                System.out.println("Player 2 wins!");
            }
            else if(player1.compareTo(player2) == 1)
            {
                System.out.println("Player 1 wins!");
            }
            else if(player1.compareTo(player2) == 0)
            {
                System.out.println("It's a tie!");
            }
            System.out.println();
        }
        System.out.print("\nRestored deck:");
        deck.restoreDeck();
        System.out.println(deck);
    }
}