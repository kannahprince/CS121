import java.util.Scanner;
import java.util.Random;

/**
 * PP 4.13: Rock, Paper, Scissors Game.
 *
 * @author
 */
public class RockPaperScissors
{
    public enum Player {COMPUTER, USER, TIE};

    public static void main(String[]args)
    {
    	boolean playing = false;
    	
        Scanner scan = new Scanner(System.in);

        Gesture computerGesture = null, userGesture = null;
        Player winner = null;

        int userWins = 0, userLosses = 0, ties = 0;
        do {
        Random randC = new Random();

        // computer chooses
        // TODO: Change the following statement so it makes
        // a random choice. (Hint: You can get the number of
        // gestures to choose from using Gesture.values().length).
        //int randC=Gesture.values().length;
        
        int computerChoice = randC.nextInt(Gesture.values().length); //use Random here

       
        // TODO: Use a switch statement to assign corresponding
        // Gesture enum value to the computerGesture variable
        switch(computerChoice){
        case 0:
        	computerGesture = Gesture.ROCK;
        	break;
        case 1:
        	computerGesture = Gesture.PAPER;
        	break;
        case 2:
        	computerGesture = Gesture.SCISSORS;
        	break;
        }
        // TODO: Print a menu of all available gestures using a
        // for-each loop that iterates over Gesture.values()
        System.out.println("Select your gesture:");
        for(Gesture move: Gesture.values())
        {
             System.out.println(move.ordinal() + ")" + move);
        }
        

        // player chooses
        int playerChoice = scan.nextInt();
        
        while(playerChoice>2 ||playerChoice <0)
        {
        	System.out.println("Please choose a valid choice");
            playerChoice = scan.nextInt();
        }
        
        
        
        // TODO: Use a switch statement to assign corresponding
        // Gesture enum value to the userGesture variable
       
        switch(playerChoice){
        case 0:
        	userGesture = Gesture.ROCK;
        	break;
        case 1:
        	userGesture = Gesture.PAPER;
        	break;
        case 2:
        	userGesture = Gesture.SCISSORS;
        }

        // clear the scan buffer
        scan.nextLine();

        System.out.println("You chose " + userGesture + ".");
        System.out.println("Computer chose " + computerGesture + ".");

        // TODO: Determine the winner. Set the winner variable
        // to the correct Player.
        // You can use nested if-else statements or nested
        // switch statements
        if (userGesture == Gesture.PAPER && computerGesture == Gesture.SCISSORS){
        	winner = Player.COMPUTER;}
        	
        if (userGesture == Gesture.ROCK && computerGesture == Gesture.PAPER){
        		winner = Player.COMPUTER;}
        
        if (userGesture == Gesture.SCISSORS && computerGesture == Gesture.ROCK){
        		winner = Player.COMPUTER;}
        
        //update user
        if (userGesture == Gesture.SCISSORS && computerGesture == Gesture.PAPER){
        	winner = Player.USER;
        }
        if (userGesture == Gesture.PAPER && computerGesture == Gesture.ROCK){
        	winner = Player.USER;
        }
        if (userGesture == Gesture.ROCK && computerGesture == Gesture.SCISSORS){
        	winner = Player.USER;
        }
        if (userGesture == computerGesture){
        	winner = Player.TIE;
        };
        

        // TODO: Print who won and increment appropriate counter.
        // Use a switch statement on the value of winner.
        // (e.g. switch(winner) { ... }).
        
        switch (winner){
        case USER:
        	System.out.println("You win!");
        	userWins++;
        	break;
        case COMPUTER:
        	System.out.println("Computer wins!");
        	userLosses++;
        	break;
        case TIE:
        	System.out.println("It's a tie");
        	ties++;
        	
        	
        }

        // TODO: Ask if they want to play again and loop back to the
        // top if they do. Print a table of final results before
        // exiting if they decide they are done playing.
     
     System.out.println("Do you want to play again?");
     String playAgain = scan.nextLine();
     if (playAgain.toLowerCase().charAt(0)== 'y'){
    	 playing = true;
     }
     if (playAgain.toLowerCase().charAt(0)=='n'){
    	 playing = false;
    	 System.out.println("You won "+ userWins + " times.");
         System.out.println("You lost "+ userLosses + " times.");
         System.out.println("We tied "+ ties + " times.");
     }
    } while(playing == true);
    scan.close();
    	
    	
    	}
}
