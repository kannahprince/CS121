/**
 * class converts input of seconds to hours, minutes and seconds.
 * @author Prince Kannah
 */

import java.util.Scanner;

public class ConvertToHours
{

	public static void main(String[] args)
	{

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the total # of seconds: ");
		int seconds = scan.nextInt();

		int toHours = (seconds / 3600);
		int toMinutes = ((seconds % 3600) / 60);
		int toSeconds = (seconds % 60);
		double hours_double = (seconds / 3600.0);

		System.out.println("Hours: " + toHours);
		System.out.println("Minutes: " + toMinutes);
		System.out.println("Seconds: " + toSeconds);
		System.out.println("seconds as fractional hour:  " + hours_double);

		scan.close();
	}

}