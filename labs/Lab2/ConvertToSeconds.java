/**
*the class prints out total number of seconds based on inputs from the user.
@author PrinceK
*/
//import Scanner class
import java.util.Scanner;


public class ConvertToSeconds{
	
  public static void main(String[] args){
	  
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter hours: " );
    int hours = scan.nextInt();
    System.out.print("Enter minutes: ");
    int minutes = scan.nextInt();
    System.out.print("Enter seconds: ");
    int seconds = scan.nextInt();
    
    int hoursToMinutes = (hours * 3600);
    int minutesToSeconds = (minutes * 60);
    int totalSeconds = hoursToMinutes + minutesToSeconds + seconds;
    System.out.println("Total # of seconds: " + totalSeconds);
    
    scan.close();
  }
}