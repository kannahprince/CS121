import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * Displays a JList of Photos and a preview of the photo.
 * This class manages layout of controls and also handles events.
 *
 * @author CS121 Instructors
 * @author You
 */
@SuppressWarnings("serial")
public class PhotoAlbumGUIPanel extends JPanel
{
	/** The data representing the list of photos in the album (the "model") */
	private PhotoAlbum album;
    PhotoListListener selectionListener;

	/** The GUI representation of the list of photos in the album (the "view") */
	private JList<Photo> photoList;

    /* LAB 12 portion */
	private JLabel imageLabel;  // Displays the image icon inside of the preview panel
	private JButton nextButton; // Selects the next image in the photoList
	private JButton prevButton; // Selects the previous image in the photoList */
    private JButton addPhoto;

    /* LAB 13 */
    private Photo[][] photoSquare;
    private JButton[][] photoSquareButtons;
    private DefaultListModel<Photo> model = new DefaultListModel<>();
    private JPanel photoGrid;


    /**
	 * Instantiates the photo album and adds all of the components to the JPanel.
	 */
	public PhotoAlbumGUIPanel() throws IOException {
        /* LAB 12 START */
        setLayout(new BorderLayout());
        JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		// Instantiate the album object and print it to the command line
		//      to make sure it worked.
		album = new PhotoAlbum("Boise", "photos/album.dat");
        for (Photo p: album.getPhotoArray()) {
            model.addElement(p);
        }
        photoList = new JList<>(model); /* This is important for updating later */
        photoList.setSelectedIndex(0);

        /* LAB 13 START */
        initGridPanel();

        /* LAB 12 START */
        selectionListener = new PhotoListListener();
        photoList.addListSelectionListener(selectionListener);
        JScrollPane pane = new JScrollPane(photoList);
        leftPanel.add(pane);
        JPanel previewPanel = new JPanel();
        imageLabel = new JLabel();
        previewPanel.add(imageLabel);
        leftPanel.add(previewPanel);
        displayPhoto(photoList.getSelectedValue());

        JPanel controlPanel = new JPanel();
        /* LAB 13 */
        addPhoto = new JButton("Add photo");
        addPhoto.addActionListener(new ControlListener());
        initGridPanel();

        /* LAB 12 */
        prevButton = new JButton("<");
        prevButton.addActionListener(new ControlListener());
        nextButton = new JButton(">");
        nextButton.addActionListener(new ControlListener());
        controlPanel.add(prevButton);
        controlPanel.add(nextButton);

        /* LAB 13 */
        controlPanel.add(addPhoto);

        /* LAB 12 */
        leftPanel.add(controlPanel);

        add(leftPanel, BorderLayout.WEST);
	}

	private class PhotoListListener implements ListSelectionListener
	{
		/* (non-Javadoc)
		 * @see java.awt.event.ListSelectionListener#valueChanged(java.awt.event.ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent event)
		{
			// photo currently selected in the photoList.
            displayPhoto(photoList.getSelectedValue());
		}
	}

	private class ControlListener implements ActionListener
	{
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent event)
		{
			// Find index of photo that is currently selected.
			int index = photoList.getSelectedIndex();
            JButton source = (JButton) event.getSource();
            if (source.equals(prevButton)) {
                index--;
                if (index == -1)
                    index = album.getNumPhotos() - 1;
            }
            else if(source.equals(nextButton)){
                index++;
                if (index == album.getNumPhotos())
                    index = 0;
            }
            else {
                showForm();
                initGridPanel();
            }
			photoList.setSelectedIndex(index);
		}
	}

	/* LAB 13 */
    private class PhotoSquareListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < photoSquareButtons.length; i++) {
                for (int j = 0; j < photoSquareButtons[i].length; j++) {
                    if (e.getSource().equals(photoSquareButtons[i][j])){
                        displayPhoto(photoSquare[i][j]);
                        photoList.setSelectedValue(photoSquare[i][j], true);
                    }
                }
            }
        }
    }
    /* LAB 13 */

	/**
	 * Updates the photo on the preview panel.
	 * @param photo The photo to display.
	 */
	private void displayPhoto(Photo photo)
	{
		try {
			ImageIcon icon = new ImageIcon(ImageIO.read(photo.getFile()));
			imageLabel.setIcon(icon);
		} catch (IOException ex) {
			imageLabel.setText("Image not found :(");
		}
	}

	/* LAB 13 */
    /**
     * Pops up an input form using a JOptionPane. Reads the data from the form
     * when the user clicks "OK". If the user clicks "CANCEL", nothing happens.
     */
    private void showForm()
    {
        JPanel formInputPanel = new JPanel();
        formInputPanel.setLayout(new BoxLayout(formInputPanel, BoxLayout.Y_AXIS));

        JTextField nameField = new JTextField(15);
        JTextField fileField = new JTextField(15);

        formInputPanel.add(new JLabel("Name: "));
        formInputPanel.add(nameField);
        formInputPanel.add(new JLabel("File: "));
        formInputPanel.add(fileField);

        int result = JOptionPane.showConfirmDialog(null, formInputPanel, "Add photo",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

        // If they click okay, then we will process the form data. The validation here isn't
        // very good. We could make sure they didn't enter an empty name. We could keep asking
        // them for valid input.
        if (result == JOptionPane.OK_OPTION)
        {
            String name = nameField.getText();
            String file = fileField.getText();

            Photo p = new Photo(name, new File(file));
            album.addPhoto(p);
            System.out.println(album);
            model.addElement(p);
        }
    }
    /* LAB 13 */

    /**
     * Helper method to setup the photo grid
     */
    private void initGridPanel(){
        if (photoGrid != null)
            this.remove(photoGrid);
        photoGrid = new JPanel();
        photoSquare = album.getPhotoSquare();
        photoSquareButtons = new JButton[photoSquare.length][photoSquare.length];
        photoGrid.setLayout(new GridLayout(photoSquareButtons.length, photoSquareButtons.length));
        for (int i = 0; i < photoSquareButtons.length; i++) {
            for (int j = 0; j < photoSquareButtons[i].length; j++) {
                photoSquareButtons[i][j] = new JButton();
                ImageIcon icon = null;
                try {
                    icon = new ImageIcon(ImageIO.read(photoSquare[i][j].getFile()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                photoSquareButtons[i][j].setIcon(icon);
                photoSquareButtons[i][j].addActionListener(new PhotoSquareListener());
                photoGrid.add(photoSquareButtons[i][j]);
            }
        }
        add(photoGrid, BorderLayout.CENTER);
        revalidate();
    }
}