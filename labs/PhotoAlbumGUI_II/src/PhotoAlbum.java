import java.util.ArrayList;
import java.util.Scanner;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Represents a photo album.
 *
 * @author CS121 Instructors
 * @author You - complete getPhotoArray() method
 */
public class PhotoAlbum
{
	private String name;
	private ArrayList<Photo> photos;

	/**
	 * Creates a new photo album with the given name from the data file located at the
	 * given file path. The album data file should have the format
	 * <pre>
	 * Photo Name 0
	 * filepath 0
	 * Photo Name 1
	 * filepath 1
	 * ...
	 * </pre>
	 * @param name The name of the album.
	 * @param filename The relative path the the file containing the photo album data.
	 */
	public PhotoAlbum(String name, String filename)
	{
		this.name = name;
		this.photos = new ArrayList<>();
		loadFromFile(filename);
	}

	/**
	 * Returns the number of photos in this album.
	 * @return the number of photos in this album.
	 */
	public int getNumPhotos()
	{
		return photos.size();
	}

	/**
	 * Returns a copy of the photos list as an array of Photos.
	 * @return an array of Photos in the album.
	 */
	public Photo[] getPhotoArray()
	{
		Photo[] copy = new Photo[photos.size()];
		for (int i = 0; i < copy.length ; i++) {
			copy[i] = photos.get(i);
		}
		return copy;
	}

    /**
     * Copy the photo album into an 2D array
     * @return the photo album as a 2D array
     */
	public Photo[][] getPhotoSquare(){
        int size = (int)Math.ceil(Math.sqrt(getNumPhotos()));
        Photo[][] copy = new Photo[size][size];
        for (int i = 0; i < copy.length; i++) {
            for (int j = 0; j < copy[i].length ; j++) {
                copy[i][j] = photos.get((i * size + j) % getNumPhotos());
            }
        }
       return copy;
    }

	/**
	 * Loads photos from the data in the given file into this photo album.
	 * @param filename path of the file to read from.
	 */
	private void loadFromFile(String filename)
	{
		File file = new File(filename);
		if(file.exists()) {
			try {
				Scanner scan = new Scanner(file);
				while(scan.hasNextLine()) {
					String photoName = scan.nextLine().trim();
					File photoFile = new File(scan.nextLine().trim());
					Photo photo = new Photo(photoName, photoFile);
					photos.add(photo);
				}
				scan.close();
			} catch(FileNotFoundException e) {
				System.err.println("Could not read album file: " + e.getMessage());
			}
		} else {
			System.err.println("Album not found:: " + file);
		}
	}

    /**
     * Add the specified photo to this album
     * @param photo the photo to add to this album
     */
    public void addPhoto(Photo photo){
        photos.add(photo);

    }

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("------ " + name + " ------\n");
		for(Photo photo : photos) {
			builder.append(photo + "\n");
		}
		return builder.toString();
	}
}