import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author rolvera
 */
public class GameBoard extends JPanel implements ActionListener
{

    private JButton[][] buttons;
    private String Player = "X";
    private String Computer = "O";
    boolean btnClick = false;
    boolean won = false;
    int turn = 1;
    int wonum = 1, wonum2 = 1, wonum3 = 1;
    final int wonCombo[][] = new int[][] {{0,1,2},{0,3,6},{0,4,8},{3,4,5},
            {1,4,7},
            {2,4,6},
            {6,7,8},
            {2,5,8}};
    String message;

    public GameBoard()
    {

        JPanel gamePanel = new JPanel();
        gamePanel.setLayout(new GridLayout(3,3));
        gamePanel.setPreferredSize(new Dimension(500,500));

        buttons = new JButton[3][3];

        for(int i = 0; i < buttons.length; i++)
        {

            for(int j = 0; j < buttons.length; j++)
            {
                buttons[i][j] = new JButton();
                buttons[i][j].addActionListener(this);
                gamePanel.add(buttons[i][j]);
                buttons[i][j].setFont(new Font("Arial",0,32));
            }

        }
        this.add(gamePanel);

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        Object source = e.getSource();

        for(int i = 0; i < buttons.length; i++)
        {
            for(int j = 0; j < buttons.length; j++)
            {
                String label = buttons[i][j].getText();
                if(source == buttons[i][j])
                {
                    buttons[i][j].setText(Player);
                    computerTurn();
                }
            }
        }

    }

    public void computerTurn()
    {
        for(int row = 0; row < buttons.length; row++)
        {
            for(int col = 0; col < buttons[row].length; col++)
            {
                if(buttons[row][col].getText() == "")
                {
                    buttons[row][col].setText(Computer);
                    return;
                }
            }
        }
    }

    //	public String checkWin() { 
    //		for(int[] combo: wonCombo){
    //			if(buttons[combo[0]/3][combo[0]%3].getText().equals(buttons[combo[1]/3][combo[1]%3].getText()) &&
    //				buttons[combo[0]/3][combo[0]%3].getText().equals(buttons[combo[2]/3][combo[2]%3].getText())){
    //				return buttons[combo[0]/3][combo[0]%3].getText();
    //			}
    //		}
    ////		FINAL INT WONCOMBO[][] = NEW INT[][] {
    ////				{0, 1, 2}, {0, 3, 6}, {0, 4, 8},
    ////				{3, 4, 5}, {1, 4, 7}, {2, 4, 6},
    ////				{6, 7, 8}, {2, 5, 8}
    ////				};
    //		for(int i=0; i<7; i++) {	
    //		if(
    //		!buttons[wonCombo[i][0]].getText().equals("") &&
    //		buttons[wonCombo[i][0]].getText().equals(buttons[wonCombo[i][1]].getText()) &&
    //		buttons[wonCombo[i][1]].getText().equals(buttons[wonCombo[i][2]].getText())) {
    //		won = true;
    //		wonum = wonCombo[i][0];
    //		wonum2 = wonCombo[i][1];
    //		wonum3 = wonCombo[i][2];
    //		break;
    //		}
    //		}
    //		if(won || (won && turn>7)) {
    //		if(won) {
    //		if(turn % 3 == 0)
    //		message = "Player has won!";
    //		else
    //		message = "Computer has won!";
    //		won = false;
    //		} else if(won && turn>7) {
    //		message = "Both players have tied!\nBetter luck next time.";
    //		}
    //		}

    // TODO Auto-generated method stub
}
