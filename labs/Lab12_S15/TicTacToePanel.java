import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class TicTacToePanel extends JPanel implements ActionListener
{
    private final int SIZE = 3;
    private JButton[][] buttons;
    private JButton newGame;
    private JPanel gameBoard;
    private JPanel cPanel, aiPanel, statsPanel;
    private ButtonGroup group;
    private JRadioButton dumb, random;
    private boolean userMove;
    private JLabel wins, looses;
    private int winCount = 0, loosesCount = 0;
    private int playerCount = 0, computerCount = 0;

    private enum Player
    {
        COMPUTER, USER;
    }

    public TicTacToePanel()
    {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(400,250));
        initButtons();
        initGameBoard();
    
        add(cPanel,BorderLayout.WEST);
        add(statsPanel,BorderLayout.SOUTH);
    }

    private void initButtons()
    {
        initAIPanel();
        initStatsPanel();
        cPanel = new JPanel();
        cPanel.setLayout(new BoxLayout(cPanel,BoxLayout.Y_AXIS));

        newGame = new JButton("NEW GAME");
        newGame.addActionListener(this);

        cPanel.add(Box.createRigidArea(new Dimension(0,30)));
        cPanel.add(newGame);
        cPanel.add(Box.createRigidArea(new Dimension(0,20)));
        cPanel.add(aiPanel);

    }

    private void initStatsPanel()
    {
        statsPanel = new JPanel();
        statsPanel.setLayout(new BoxLayout(statsPanel,BoxLayout.X_AXIS));

        wins = new JLabel("WINS: " + winCount);
        looses = new JLabel("LOOSES: " + loosesCount);

        statsPanel.add(Box.createRigidArea(new Dimension(100,0)));
        statsPanel.add(wins);
        statsPanel.add(Box.createRigidArea(new Dimension(30,0)));
        statsPanel.add(looses);
    }

    private void initAIPanel()
    {
        aiPanel = new JPanel();
        aiPanel.setLayout(new BoxLayout(aiPanel,BoxLayout.Y_AXIS));
        aiPanel.setBorder(BorderFactory.createTitledBorder("AI"));

        //create the radio buttons
        dumb = new JRadioButton("DUMB");
        dumb.addActionListener(this);

        random = new JRadioButton("RANDOM");
        random.setToolTipText("How you like them apples");
        random.addActionListener(this);

        //add the to the group
        group = new ButtonGroup();
        group.add(dumb);
        group.add(random);

        aiPanel.add(dumb);
        aiPanel.add(random);
    }

    private void initGameBoard()
    {
        if(gameBoard != null)
        {
            this.remove(gameBoard);
        }
        gameBoard = new JPanel();
        gameBoard.setPreferredSize(new Dimension(300,200));
        gameBoard.setLayout(new GridLayout(SIZE,SIZE,2,2));

        buttons = new JButton[SIZE][SIZE];

        for(int p = 0; p < buttons.length; p++)
        {
            for(int j = 0; j < buttons[p].length; j++)
            {
                buttons[p][j] = new JButton();
                buttons[p][j].addActionListener(this);
                buttons[p][j].setFont(new Font("Arial",0,32));
                buttons[p][j].setHorizontalAlignment(SwingConstants.CENTER);
                gameBoard.add(buttons[p][j]);
            }
        }
        add(gameBoard,BorderLayout.CENTER);
        this.revalidate();

    }

    /**
     * In this method, the computer picks the fist available open spot.
     * @param i
     * @param j
     */
    private void dumbAi(int i, int j)
    {
        if(buttons[i][j].getText().equalsIgnoreCase("")
                && !buttons[i][j].getText().equalsIgnoreCase("x") && userMove)
        {
            buttons[i][j].setText("O");
            buttons[i][j].removeActionListener(this);
            userMove = false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(newGame))
        {
            initGameBoard();
        }
        else
        {
            for(int i = 0; i < buttons.length; i++)
            {
                for(int j = 0; j < buttons[i].length; j++)
                {
                    if(e.getSource().equals(buttons[i][j])
                            && !buttons[i][j].getText().equalsIgnoreCase("o"))
                    {
                        buttons[i][j].setText("X");
                        buttons[i][j].removeActionListener(this);
                        userMove = true;
                    }
                    dumbAi(i,j);
                }
            }

            if(checkRow() == Player.USER)
            {
                playerCount++;
            }
            else
            {
                computerCount++;

            }

            if(checkCol() == Player.USER)
            {
                playerCount++;
            }
            else
            {
                computerCount++;

            }
            
            System.out.println("Player count: " + playerCount + " computerCount: "
                    + computerCount);
        }
    }

    private Player checkRow()
    {
        for(int i = 0; i < buttons.length; i++)
        {
            if(buttons[0][i].getText().equalsIgnoreCase("x"))
            {
                return Player.USER;

            }
            else if(buttons[0][i].getText().equalsIgnoreCase("o"))
            {
                return Player.COMPUTER;
            }
        }
        return null;

    }

    private Player checkCol()
    {
        for(int i = 0; i < buttons[i].length - 1; i++)
        {
            if(buttons[i][0].getText().equalsIgnoreCase("x"))
            {
                return Player.USER;
            }
            else if(buttons[i][0].getText().equalsIgnoreCase("o"))
            {
                return Player.COMPUTER;
            }

        }
        return null;
    }

    public static void main(String[] args)
    {

        ButtonGroup bGroup = new ButtonGroup();

        bGroup.add(new JRadioButton("HELL"));
        bGroup.add(new JRadioButton("HEAVEN"));

        JFrame frame = new JFrame("Tic Tac Toe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.add(new TicTacToePanel());

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
