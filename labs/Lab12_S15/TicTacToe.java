import javax.swing.JFrame;

/**
 * @author rolvera
 */
public class TicTacToe
{

    public static void main(String[] args)
    {
        JFrame f = new JFrame("TicTacToe");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLocationRelativeTo(null);//centers frame on screen

        //Create a TextButtonsPanel to display the buttons and textArea
        GameBoard mainPanel = new GameBoard();

        f.getContentPane().add(mainPanel);
        f.pack();

        f.setVisible(true);
    }
}
