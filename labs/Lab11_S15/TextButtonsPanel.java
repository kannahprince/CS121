import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Displays a grid of JButtons and a JTextArea. Together, they act as a very
 * small keyboard and text display. This class manages layout of controls and
 * also handles events.
 * @author mvail
 */
@SuppressWarnings("serial")
public class TextButtonsPanel extends JPanel implements ActionListener
{
    private JButton[] buttons;//Do not change
    private JPanel grid;
    private JTextArea textArea;//Do not change
    //Assign values for these constants in the constructor
    private JButton ENTER;
    private JButton SPACE;
    private JButton CLEAR;

    /**
     * Constructor
     */
    public TextButtonsPanel()
    {
        //TODO: Create and populate the buttons array
        buttons = new JButton[12];
        grid = new JPanel();
        grid.setLayout(new GridLayout(4,3));

        for(int i = 1; i < buttons.length-3; i++)
        {
            buttons[i] = new JButton(Integer.toString(i));
            buttons[i].addActionListener(this);
            grid.add(buttons[i]);
        }

        ENTER = new JButton("ENTER");
        ENTER.addActionListener(this);
        grid.add(ENTER);

        SPACE = new JButton("SPACE");
        SPACE.addActionListener(this);
        grid.add(SPACE);

        CLEAR = new JButton("CLEAR");
        CLEAR.addActionListener(this);
        grid.add(CLEAR);

        //TODO: Create the text area - set its editable property to false
        textArea = new JTextArea();
        textArea.setEditable(false);

        //TODO: Add the grid panel to this panel
        add(grid);

        JScrollPane pane = new JScrollPane(textArea);
        pane.setPreferredSize(new Dimension(110,200));
        add(pane);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent event)
    {
        if(event.getSource().equals(ENTER))
        {
            textArea.append("\n");
        }
        else if(event.getSource().equals(CLEAR))
        {
            textArea.setText("");
        }
        else if(event.getSource().equals(SPACE))
        {
            textArea.append(" ");
        }
        else
        {
            for(int i = 0; i < buttons.length; i++)
            {
                if(event.getSource().equals(buttons[i]))
                {
                    textArea.append(buttons[i].getText());
                }
            }
        }
    }
    
    
    /**
     * Create the JFrame 
     * @param args not used
     */
    public static void main(String[] args) {
            JFrame f = new JFrame("Text Buttons");
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.setLocationRelativeTo(null); //centers frame on screen

            //Create a TextButtonsPanel to display the buttons and textArea
            TextButtonsPanel mainPanel = new TextButtonsPanel();
            
            f.getContentPane().add(mainPanel);
            f.pack();

            f.setVisible(true);
    }
}
