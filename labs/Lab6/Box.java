
/**
 * The <code>Box</code> class represents a box. Each box has a width, height,
 * depth, and status (i.e full or empty). Here is an example of how a box can be
 * created.
 * <p>
 * <blockquote>
 * 
 * <pre>
 * 
 * Box box = new Box(4,5,2);
 * </pre>
 * 
 * </blockquote>
 * <p>
 * Here is an example of how a Box can be used.
 * <p>
 * <blockquote>
 * 
 * <pre>
 * System.out.println("Box's depth is: " + box.getDepth());
 * System.out.println(box);
 * </pre>
 * 
 * </blockquote>
 * <p>
 * @author Prince Kannah
 */

public class Box
{
    private double width;
    private double height;
    private double depth;
    private boolean isEmpty;
    private ShippingLabel label;

    /**
     * Constructor: Creates a new Box with the specified parameters.
     * @param widthValue box's width
     * @param heightValue box's height
     * @param depthValue box's depth
     */
    public Box(double widthValue, double heightValue, double depthValue)
    {
        this.width = widthValue;
        this.height = heightValue;
        this.depth = depthValue;
        isEmpty = false;
        label = null;
    }

    /**
     * Constructor: Creates a new Box with the specified parameters.
     * @param widthValue box's width
     * @param heightValue box's height
     * @param depthValue box's depth
     * @param isEmpty the box's status (i.e. full or empty)
     */
    public Box(double widthValue, double heightValue, double depthValue,
            boolean isEmpty)
    {
        this.width = widthValue;
        this.height = heightValue;
        this.depth = depthValue;
        this.isEmpty = isEmpty;
    }

    /**
     * Returns the width of this <code>Box</code>
     * @return the width
     */
    public double getWidth()
    {
        return width;
    }

    /**
     * Returns the height of this <code>Box</code>
     * @return the height
     */
    public double getHeight()
    {
        return height;
    }

    /**
     * Returns the depth of this <cod>Box</code>
     * @return the depth
     */
    public double getDepth()
    {
        return depth;
    }

    /**
     * Returns the status of this <code>Box</code> (i.e full or empty)
     * @return the status
     */
    public boolean getIsEmpty()
    {
        return isEmpty;
    }

    /**
     * Sets status to new value
     * @param isEmpty new status
     */
    public void setIsEmpty(boolean isEmpty)
    {
        this.isEmpty = isEmpty;
    }

    /**
     * Calculate and return the volume of this <code>Box</code>
     * @return the volume
     */
    public double volume()
    {
        return(width * height * depth);

    }

    /**
     * Calculate and return the surface are of this <code>Box</code>
     * @return the surface area
     */
    public double surfaceArea()
    {
        //total surface area
        return((2 * width * depth) + (2 * depth * height)
                + (2 * width * height));
    }

    /**
     * Sets width to new value
     * @param newWidth new width
     */
    public void setWidth(double newWidth)
    {
        width = newWidth;
    }

    /**
     * Sets height to new value
     * @param newHeight new height
     */
    public void setHeight(double newHeight)
    {
        height = newHeight;
    }

    /**
     * Sets depth to new value
     * @param newDepth new depth
     */
    public void setDepth(double newDepth)
    {
        depth = newDepth;
    }

    public ShippingLabel getLabel()
    {
        return label;
    }

    public void setLabel(ShippingLabel label)
    {
        this.label = label;
    }

    public String toString()
    {
        return String.format("%-5s %.2f %-5s %.2f %-5s %.2f %-5s %.2f","Width: ",
                getWidth(),"\tHeight:",getHeight(),"\tVolume:",volume(),
                "\tSurface area:",surfaceArea());
    }
}
