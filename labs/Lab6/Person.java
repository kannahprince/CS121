
public class Person
{

    private String firstName, lastName;
    private int age;

    public Person(String firstName, String lastName, int age)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getfirstName()
    {
        return firstName;
    }

    public void setfirstName(String name)
    {
        this.firstName = name;
    }

    /**
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    /**
     * @return the age
     */
    public int getAge()
    {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age)
    {
        this.age = age;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "Person [firstName=" + firstName + ", lastName=" + lastName
                + ", age=" + age + "]";
    }

    public static void main(String[] args)
    {
        Person me = new Person("Prince","Kannah",21);
        System.out.println(me);

        me.setAge(22);
        me.setLastName("Duepa");

        System.out.println(me);

        System.out.println("Hello, my name is " + me.getfirstName() + " "
                + me.getLastName() + " and you'll find me under:\n"
                + me.getfirstName().charAt(0) + ". " + me.getLastName());

    }

}
