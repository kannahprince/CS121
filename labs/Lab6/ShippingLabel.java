import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Random;

public class ShippingLabel
{
    private final Random RAND = new Random();
    private static double charge = 0.25;
    private String address;
    private long label;

    public ShippingLabel(String address)
    {
        this.address = address;
       label = Math.abs(RAND.nextLong());
    }

//    public ShippingLabel(String address)
//    {
//        this.address = address;
//    }
    public void setLabel(long label){
        this.label = Math.abs(label);
    }
    
    public long getLabel(){
       return label;
    }

    public static double getCost(double volume)
    {
        return volume * charge;
    }

    @Override
    public String toString()
    {
        DecimalFormatSymbols labelSymbols = new DecimalFormatSymbols();  

        // Use space not comma to thousands: 10 000 not 10,000.   
        labelSymbols.setGroupingSeparator('-');   

        DecimalFormat labelFormatter = new DecimalFormat("####,###,###", labelSymbols);
        return "Address: " + address + "\nShipping label#: " + labelFormatter.format(label);
    }

}
