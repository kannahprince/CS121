import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class BoxTest
{
    public static void main(String[] args)
    {
        // new box
        //        Box smallBox = new Box(4,5,2);
        //        System.out.println(smallBox + "\n");
        //
        //        System.out.println("Getter methods with initial values:");
        //        System.out.println("The depth is: " + smallBox.getDepth());
        //        System.out.println("The height is: " + smallBox.getHeight());
        //        System.out.println("The width is : " + smallBox.getWidth());
        //        System.out.println("The box is empty: " + smallBox.getIsEmpty());
        //        System.out.println("The surface area is: " + smallBox.surfaceArea());
        //        System.out.println("The volume is: " + smallBox.volume());
        //
        //        // setter methods
        //        System.out.println("\nsmallBox with setter methods");
        //        smallBox.setIsEmpty(false);
        //        smallBox.setWidth(2.0);
        //        smallBox.setHeight(3.0);
        //        smallBox.setDepth(1.0);
        //        System.out.println(smallBox + "\n");

        // now the real fun begins
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);

        System.out.print("How many boxs would you like to create: ");
        int numBox = scan.nextInt();
        //space
        System.out.println();

        while (numBox <= 0)
        {
            System.out.print("Number of boxes must be positive: ");
            numBox = scan.nextInt();
        }

        scan.close();

        double largestVolume = Double.MIN_VALUE;
        ArrayList<Box> boxCollection = new ArrayList<Box>();

        for(int i = 1; i < numBox; i++)
        {
            Box tempBox = new Box(rand.nextDouble() * 10.0,
                    rand.nextDouble() * 10.0,rand.nextDouble() * 10.0,
                    rand.nextBoolean());
            boxCollection.add(tempBox);
            System.out.println("Box " + (1+i) + ": " + tempBox);
        }

        int bIndex = 0;
        for(Box b : boxCollection)
        {

            if(b.volume() >= largestVolume)
            {
                largestVolume = b.volume();
                bIndex = boxCollection.indexOf(b) + 1;
            }
        }
        System.out.println("\nThe largest box is Box #" + bIndex
                + " with a volume of: " + String.format("%.2f",largestVolume));
    }

}
