import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * PlanetExpress is a shipping program.
 * @author pkannah
 *
 */
public class PlanetExpress
{

    private static Random rand = new Random();
    private static Scanner scan = new Scanner(System.in);

    /**
     * Enum used to represent possible user selection options
     * @author pkannah
     *
     */
    private enum CUSTOMER_OPTIONS
    {
        ORDER_A_BOX, CHECK_ON_A_ORDER
    };

    /**
     * main method
     * @param args (not use)
     */
    public static void main(String[] args)
    {
        System.out.println(
                "Welcome to Planet Express. Your #1 interplanetary destination for all your boxing needs!\n\n"
                        + "What can we do for you today?:");

        switch (getUserChoice())
        {
            case 0:
                orderBox();
            break;
            case 1:
                checkOrder();
            break;
            default:
                getUserChoice();
        }
    }

    /**
     *Prints possible user choice then uses scanner to get selection 
     * @return the user's selection
     */
    private static int getUserChoice()
    {
        int userC;
        for(CUSTOMER_OPTIONS s : CUSTOMER_OPTIONS.values())
        {
            System.out.println(
                    s.ordinal() + "): " + s.toString().replaceAll("_"," "));
        }

        userC = scan.nextInt();

        while (userC > CUSTOMER_OPTIONS.values().length - 1 || userC < 0)
        {
            System.out.println("Please choose a valid choice:");

            for(CUSTOMER_OPTIONS s : CUSTOMER_OPTIONS.values())
            {
                System.out.println(
                        s.ordinal() + "): " + s.toString().replaceAll("_"," "));
            }
            userC = scan.nextInt();
        }
        return userC;
    }

    /**
     * Code that's executed when user selects to order a box
     */
    private static void orderBox()
    {
        System.out.println("Great! How big?");
        Box customBox = createBox();
        double cost = ShippingLabel.getCost(customBox.volume());

        System.out.println("Your custom box: " + customBox + "\n");
        System.out.print("One box is going to cost: "
                + NumberFormat.getCurrencyInstance().format(cost)
                + "\nHow many would you like to order?: ");

        long quantity = scan.nextLong();
        scan.nextLine();//clear buffer
        System.out.println("\nYour total is: "
                + NumberFormat.getCurrencyInstance().format((quantity * cost)));

        System.out.print("Enter full shipping address: ");
        customBox.setLabel(new ShippingLabel(scan.nextLine()));

        System.out.println("\nAwesome! Your box will be shipped to: \n"
                + customBox.getLabel());
    }
    
    /**
     * Code that's executed when user selects to check a order
     */
    @SuppressWarnings("unused")
    private static void checkOrder()
    {
        System.out.print("Alright! What is Shipping Label number?: ");
        long label = scan.nextLong();

        ArrayList<Box> boxCollection = new ArrayList<Box>();

        for(int i = 1; i <= 100; i++)
        {

            boxCollection.add(new Box(rand.nextDouble() * 10,
                    rand.nextDouble() * 10,rand.nextDouble() * 10));
            //            boxCollection.get(i).setIsEmpty();

        }
        System.out.println("Your box is:\n"
                + boxCollection.get(rand.nextInt(boxCollection.size()))
                + "\nYou should recieve it in 10 days");
    }

    /**
     * Use to create custom Box with user provided dimensions
     * @return a new Box with user specified dimensions.
     */
    private static Box createBox()
    {
        System.out.print("Enter width: ");
        double width = scan.nextDouble();

        System.out.print("Enter height: ");
        double height = scan.nextDouble();

        System.out.print("Enter depth: ");
        double depth = scan.nextDouble();

        return new Box(width,height,depth);
    }
}