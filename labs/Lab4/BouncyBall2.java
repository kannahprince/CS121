import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * Animated program with a ball bouncing off the program boundaries
 * @author mvail
 * @author yourname
 */
public class BouncyBall2 extends JPanel
{

    private final int INIT_WIDTH = 600;
    private final int INIT_HEIGHT = 400;
    private final int DELAY = 10;// milliseconds between Timer events
    private Random rand;//random number generator
    private int ballOneX, ballOneY;//anchor point coordinates
    private int ballTwoX, ballTwoY;
    private int ballOneXDelta, ballOneYDelta;//change in x and y from one step to the next
    private int ballTwoXDelta, ballTwoYDelta;
    private final int DELTA_RANGE = 20;//range for xDelta and yDelta
    private int radius;//circle radius
    private int radDelta = 10;
    private final int COLOR_DELTA= 50;
    private final int COLOR_MAX = 255;

    /**
     * Draws a filled oval with random color and dimensions.
     * @param g Graphics context
     * @return none
     */
    public void paintComponent(Graphics g)
    {
        int width = getWidth();
        int height = getHeight();
        int rVal = 5; int gVal = 5; int bVal = 5;

        radius += radDelta;
        //CALCULATE NEW X
        ballOneX += ballOneXDelta;
        ballTwoX += ballTwoXDelta;

        /*
         * RIGHT EDGE:
         * First check to see if ball is on right edge
         * if so, push it back in.
         */
        if(ballOneX + radius >= width)
        {
            ballOneXDelta *= -1;
            ballOneX = width - radius;
        }

        /*
         * LEFT EDGE: Check to see if the ball is at the left edge if so, push
         * it back in
         */
        else if(ballOneX - radius <= 0)
        {
            ballOneXDelta *= -1;
            ballOneX = radius;
        }

        //ball two X
        if(ballTwoX + radius >= width) //right edge
        {
            ballTwoXDelta *= -1;
        }
        else if(ballTwoX - radius <= 0) //left edge
        {
            ballTwoXDelta *= -1;
        }

        //CALCULATE NEW Y
        ballOneY += ballOneYDelta;
        ballTwoY += ballTwoYDelta;

        /*
         * UPPER EDGE: First, check to see if (y - RADIUS) is less or equal to
         * 0, meaning the ball is at the upper edge. If that's the case, we make
         * yDelta negative, causing it move backwards.
         */
        if(ballOneY + radius >= height)
        {
            ballOneYDelta *= -1;
            ballOneY = height - radius;
        }

        /*
         * BOTTOM EDGE: Again, first we check to see if (y + RADIUS) greater
         * than or equal to the height, meaning ball is at bottom edge. If that
         * is the case, we push it back in by making yDelta, the change from one
         * step to the next, negative (kind of like walking backwards).
         */
        else if(ballOneY - radius <= 0)
        {
            ballOneYDelta *= -1;
            ballOneY = radius;
        }
        
        if(radius >= width / 5)
        {
            radDelta = -2;
        }
        else if(radius <= width/20)
        {
            radDelta = 2;
        }
        
        //BALL TWO
        if(ballTwoY - radius <= 0) //bottom edge
        {
            ballTwoYDelta *= -1;
            ballTwoY = radius;
        }
        else if(ballTwoY + radius >= height) //top edge
        {
            ballTwoYDelta *= -1;
            ballTwoY = height - radius;
        }

        if(rVal > COLOR_MAX){
            rVal = 5;
        }
        
        if(gVal > COLOR_MAX){
            gVal = 5;
        }
        if(bVal > COLOR_MAX){
            bVal = 5;
        }
        
        while(rVal < COLOR_MAX){
            rVal += COLOR_DELTA;
        }
        
        while(gVal < COLOR_MAX){
            gVal += COLOR_DELTA;
        }
        
        while(bVal < COLOR_MAX){
            bVal += COLOR_DELTA;
        }
        //NOW PAINT THE OVAL
        g.setColor(new Color(rVal,gVal,bVal));
        g.fillOval((ballOneX - radius),(ballOneY - radius),radius *2,radius *2);

        //BALL TWO        
//       g.setColor(Color.RED);
//        g.fillOval((ballTwoX - radius),(ballTwoY - radius),(radius * 2),(radius * 2));

        //Makes the animation smoother
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * Constructor for the display panel initializes necessary variables. Only
     * called once, when the program first begins. This method also sets up a
     * Timer that will call paint() with frequency specified by the DELAY
     * constant.
     */
    public BouncyBall2()
    {
        setPreferredSize(new Dimension(INIT_WIDTH,INIT_HEIGHT));
        this.setDoubleBuffered(true);
        setBackground(Color.black);

        rand = new Random();//instance variable for reuse in paint()

        //initial ball location within panel bounds
//        ballOneX = INIT_WIDTH / 2;
//        ballOneY = INIT_HEIGHT / 2;

        //ball two
        ballTwoX = INIT_WIDTH / 3;
        ballTwoY = INIT_WIDTH / 3;
        
        //TODO: replace centered starting point with a random
        // position anywhere in-bounds - the ball should never
        // extend out of bounds, so you'll need to take RADIUS
        // into account
        
//        ballOneXDelta = 5;
//        ballOneYDelta = 5;
        ballOneX = rand.nextInt(INIT_WIDTH - (2* radius)) + radius;
        ballOneY = rand.nextInt(INIT_HEIGHT - (2 *radius)) + radius;
        
        //BALL TWO:
        //deltas for x and y
//        ballTwoXDelta = 10;
//        ballTwoYDelta = 10;
        ballTwoX = rand.nextInt(INIT_WIDTH - (2* radius)) + radius;
        ballTwoY = rand.nextInt(INIT_HEIGHT - (2 *radius)) + radius;

        //TODO: replace with random deltas from -DELTA_RANGE/2
        // to +DELTA_RANGE/2
        ballOneXDelta = rand.nextInt(DELTA_RANGE/2) - DELTA_RANGE /2;
        ballOneYDelta = rand.nextInt(DELTA_RANGE/2)- DELTA_RANGE /2;
        
        //BALL TWO:
//        ballTwoXDelta = ((rand.nextInt(DELTA_RANGE/2) - DELTA_RANGE)*5) /2;
//        ballTwoYDelta = ((rand.nextInt(DELTA_RANGE/2) - DELTA_RANGE)*5) /2;

        //Start the animation - DO NOT REMOVE
        startAnimation();
    }

    /**
     * Create an animation thread that runs periodically DO NOT MODIFY
     */
    private void startAnimation()
    {
        ActionListener taskPerformer = new ActionListener() {

            public void actionPerformed(ActionEvent evt)
            {
                repaint();
            }
        };
        new Timer(DELAY,taskPerformer).start();
    }

    /**
     * Starting point for the BouncyBall program DO NOT MODIFY
     * @param args unused
     */
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("Bouncy Ball");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new BouncyBall2());
        frame.pack();
        frame.setVisible(true);
    }

}
