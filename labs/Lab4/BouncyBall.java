
//
// BouncyBall.java
//
// Example using Random and conditional statements
// CS 121
//

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * Animated program with a ball bouncing off the program boundaries
 * @author mvail
 */
public class BouncyBall extends JPanel
{

    private final int INIT_WIDTH = 600;
    private final int INIT_HEIGHT = 400;
    private final int DELAY = 100;// milliseconds between Timer events
    private Random rand;//random number generator
    private int x, y;//anchor point coordinates
    private int xDelta, yDelta;//change in x and y from one step to the next
    private final int DELTA_RANGE = 20;//range for xDelta and yDelta
    private int radius;//circle radius
    private int radDelta = 10;

    /**
     * Draws a filled oval with random color and dimensions.
     * @param canvas Graphics context
     * @return none
     */
    public void paintComponent(Graphics canvas)
    {
        int width = getWidth();
        int height = getHeight();

        //getting random colors
        int red = rand.nextInt(256);
        int green = rand.nextInt(256);
        int blue = rand.nextInt(256);
        Color randColors = new Color(blue,green,red);

        radius += radDelta;

        //clear canvas
        //canvas.setColor(getBackground());
        //canvas.fillRect(0, 0, width, height);

        //CALCULATE NEW X
        x += xDelta;
        //TODO: needs more to stay in-bounds

        //right edge
        if(x + radius >= width)
        {
            xDelta *= -1;
            x = width - radius;
        }

        //left edge
        if(x - radius < 0)
        {
            xDelta *= -1;
            x = radius;
        }
        //CALCULATE NEW Y
        y += yDelta;
        //TODO: needs more to stay in-bounds

        //upper edge
        if(y + radius >= height)
        {
            yDelta *= -1;
            y = height - radius;
        }
        //bottom edge
        if(y - radius <= 0)
        {
            yDelta *= -1;
            y = radius;

        }

        //control growth
        if(radius >= width / 5)
        {
            radDelta = -2;
        }
        else if(radius <= width / 20)
        {
            radDelta = 2;
        }
        //randomR = rand.nextInt(INIT_WIDTH/2)+5;
        //NOW PAINT THE OVAL
        canvas.setColor(randColors);
        canvas.fillOval(x - radius,y - radius,2 * radius,2 * radius);

        //Makes the animation smoother
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * Constructor for the display panel initializes necessary variables. Only
     * called once, when the program first begins. This method also sets up a
     * Timer that will call paint() with frequency specified by the DELAY
     * constant.
     */
    public BouncyBall()
    {
        setPreferredSize(new Dimension(INIT_WIDTH,INIT_HEIGHT));
        this.setDoubleBuffered(true);
        setBackground(Color.black);

        rand = new Random();//instance variable for reuse in paint()

        //initial ball location within panel bounds

        //TODO: replace centered starting point with a random
        // position anywhere in-bounds - the ball should never
        // extend out of bounds, so you'll need to take RADIUS
        // into account
        x = rand.nextInt(INIT_WIDTH - 2 * radius) + radius;
        y = rand.nextInt(INIT_HEIGHT - 2 * radius) + radius;
        //deltas for x and y
        //xDelta =5
        //yDelta = 6
        //TODO: replace with random deltas from -DELTA_RANGE/2
        // to +DELTA_RANGE/2
       
        xDelta = rand.nextInt(DELTA_RANGE/2) - DELTA_RANGE / 2;
        yDelta = rand.nextInt(DELTA_RANGE/2) - DELTA_RANGE / 2;

        //Start the animation - DO NOT REMOVE
        startAnimation();
    }

    /**
     * Create an animation thread that runs periodically DO NOT MODIFY
     */
    private void startAnimation()
    {
        ActionListener taskPerformer = new ActionListener() {

            public void actionPerformed(ActionEvent evt)
            {
                repaint();
            }
        };
        new Timer(DELAY,taskPerformer).start();
    }

    /**
     * Starting point for the BouncyBall program DO NOT MODIFY
     * @param args unused
     */
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("Bouncy Ball");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new BouncyBall());
        frame.pack();
        frame.setVisible(true);
    }

}
