
public class CardDealer {
	private static DeckOfCards deck;
	
	public static void main(String[] args) {
		 deck = new DeckOfCards();
//		 System.out.println();
		System.out.println("Fresh, unshuffled deck:" + deck);
		
		deck.shuffle();
		System.out.println("\nShuffled deck:" + deck);
		
		Card npc1 = null;
		Card npc2 = null;
		
		
		
		do{
			System.out.println("\nDrawing cards...");
			npc1 = deck.draw();
			npc2 = deck.draw();
			
			System.out.println("Player 1: " + npc1 + "\nPlayer 2: " + npc2);
			
			if(npc1.compareTo(npc2) == -1){
				System.out.println("Player 2 wins!");
			}
			else if(npc1.compareTo(npc2) == 1){
				System.out.println("Player 1 wins!");
			}
			else{
				System.out.println("It's a tie!");
			}
			
		}while(deck.numCardsRemaining() != 0);
		
		deck.restoreDeck();
		System.out.println("\nRestored deck: " + deck);

	}

}
