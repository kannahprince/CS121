import java.util.Random;

public class DeckOfCards implements DeckOfCardsInterface
{
    private final int DECKSIZE = 52;
    private int nextCardIndex;
    private Card deck[];

    public DeckOfCards()
    {
        restoreDeck();
    }

    @Override
    public void shuffle()
    {
        nextCardIndex = 0;
        Random rand = new Random();

        for(int i = 0; i < deck.length; i++)
        {
            int j = rand.nextInt(deck.length);
            Card temp = deck[i];
            deck[i] = deck[j];
            deck[j] = temp;
        }
    }

    @Override
    public Card draw()
    {

        if(nextCardIndex >= deck.length)
        {
            return null;
        }
        Card draw = deck[nextCardIndex];
        nextCardIndex++;

        return draw;
    }

    @Override
    public void restoreDeck()
    {
        nextCardIndex = 0;
        deck = new Card[DECKSIZE];
        for(int i = 0; i < deck.length;)
        {
            for(Suit suit : Suit.values())
            {
                for(FaceValue value : FaceValue.values())
                {
                    deck[i] = new Card(suit,value);
                    i++;
                }
            }
        }
    }

    @Override
    public int numCardsRemaining()
    {
        return deck.length - nextCardIndex;
    }

    @Override
    public int numCardsDealt()
    {
        return nextCardIndex;
    }

    @Override
    public Card[] dealtCards()
    {
        Card[] dealtDeck = new Card[nextCardIndex];

        for(int i = 0; i < dealtDeck.length; i++)
        {
            dealtDeck[i] = deck[i];
        }
        return dealtDeck;
    }

    @Override
    public Card[] remainingCards()
    {
        Card[] remainingDeck = new Card[numCardsRemaining()];

        for(int i = nextCardIndex; i < deck.length; i++)
        {
            remainingDeck[i] = deck[i];
        }
        return remainingDeck;
    }

    public String toString()
    {
        String finalString = "";
        for(int i = 0; i < deck.length; i++)
        {

            if(i % 4 == 0)
            {
                finalString += "\n";
            }

            finalString += " " + deck[i].getFaceValue() + " of "
                    + deck[i].getSuit().getSymbol() + " ";
        }
        return finalString;
    }

}
