# CS121: Java I
This repository contains all my labs and projects from CS121 (SP15). All programs were written in Java version 7 and have been tested on Java version 8.

More information about the class itself can be found on its [website](http://cs.boisestate.edu/~cs121/index.php).

The weekly labs that are required for CS121 lives [here](http://cs.boisestate.edu/~cs121/labs.php).
